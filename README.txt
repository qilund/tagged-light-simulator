# README #


### What is this repository for? ###
This is the tagged Light Simulator repository which can be used to simulate acousto optic interactions in scattering media with arbitrary ultrasonic fields.

This is the second version of the tagged light simulator. The simulator is updated from the previous version to have a more flexible way of describing the ultrasound and vastly speed up the performance. A dedicated Monte-Carlo simulator has also been uploaded (multiphoton) which can be used to produce photon paths.

### How do I get set up? ###

To set up, simply add the parent directories "multiphoton", "TaggingFunctions" to the path. Add all folders and subfolders of "Third_party_code" to the path. There are some examples to help you with designing your own simulation.

### Known issues ###

Some subroutines of "multiphoton" have been implemented in mex to make them faster. A known issue is that sometimes, these mex files do not work on another machine. I have included a couple of scripts in the "multiphoton" directory, which compiles the three currently used mex files. You can  open the different codegen projects in the matlab coder.

By changing the comments in the switch block in the multiphoton.m function, you can return to the "non-mex" versions of the code. This is useful if you want to add something or look for bugs (though then you also need change the parfor loops to normal for loops).

### Contribution guidelines ###

Contact dr.david.hill@pm.me if you would like to add anything.

### Who do I talk to? ###

Contact dr.david.hill@pm.me if you have any questions on this simulator.