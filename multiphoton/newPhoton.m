function [c,k] = newPhoton(source)
switch source.type
    case 'xy tophat'
        xi1 = rand;
        xi2 = rand;
        c = [sqrt(xi1)*cos(xi2*2*pi),...
            sqrt(xi1)*sin(xi2*2*pi),...
            0]*source.radius+source.center;
        k = source.start_k;
    case 'point'
        c = source.start_c;
        k = source.start_k;
    otherwise
        error('Unimplemented source type')
end

norm = sqrt(k(1).^2+k(2).^2+k(3).^2);
k = k./norm;
end
