function plotpath(fotPath,edges,plotGridBoolean,alpha)
if nargin <4
    alpha  = 1;
end

if iscell(fotPath)

    N = numel(fotPath);
    C = linspecer(N);
    ax = gca;
    set(gca,'ColorOrder',C)
    for i = 1:numel(fotPath)
        lh = plot3(  fotPath{i}(:,1),...
            fotPath{i}(:,2),...
            fotPath{i}(:,3),'LineWidth',1);
        lh.Color = [lh.Color alpha];
        hold on
        % 		plot3(  fotPath{i}(end,1),...
        % 			fotPath{i}(end,2),...
        % 			fotPath{i}(end,3),'ro')
    end
else
    plot3(  fotPath(:,1),...
        fotPath(:,2),...
        fotPath(:,3),'LineWidth',2)
    hold on
end
set(gca,'ZDir','reverse')
drawnow
box on
axis equal
if nargin > 1
    if nargin == 2
        plotGridBoolean = 1;
    end
    xlabel('x')
    ylabel('y')
    zlabel('z')
    if plotGridBoolean
        for i = 1:numel(edges.x)
            for j = 1:numel(edges.y)

                plot3([edges.x(i),edges.x(i)],...
                    [edges.y(j),edges.y(j)],...
                    [edges.z(1),edges.z(end)],'k');

            end
        end
        for i = 1:numel(edges.y)
            for j = 1:numel(edges.z)
                plot3([edges.x(1),edges.x(end)],...
                    [edges.y(i),edges.y(i)],...
                    [edges.z(j),edges.z(j)],'k');
            end
        end
        for i = 1:numel(edges.z)
            for j = 1:numel(edges.x)
                plot3([edges.x(j),edges.x(j)],...
                    [edges.y(1),edges.y(end)],...
                    [edges.z(i),edges.z(i)],'k');
            end
        end
    else
        axis([edges.x(1) edges.x(end) edges.y(1) edges.y(end) edges.z(1) edges.z(end)])
    end

end % if nargin == 2
end