% test110,test of case 110 function in multiphoton. 
%
% this script is used to generate the mex version of said case function

clearvars

%% define source with correct template
sourceTemplate = struct('type',''...
    ,'center',''  ...
    ,'radius',''  ...
    ,'start_k','' ...
    ,'start_c','' ...
    );

source.type = 'point';
source.center = [0 0 0];
source.radius = 0.5;
source.start_k = [0 0 1];
source.start_c = [0 0 0];
source = orderfields(source,sourceTemplate);

%% define path criteria with correct template
pcTemplate = struct('type',''...
    ,'radius',''  ...
    ,'coordinate',[nan nan nan]  ...
    ,'minZ',nan...
    );
pathCriteria.type = 'single voxel';
pathCriteria.radius = 0.5;
pathCriteria.coordinate = [0.8 0 1];
pathCriteria.minZ = 0;
pathCriteria = orderfields(pathCriteria,pcTemplate);

%% define detector with correct template
detectorTemplate = struct('type',''...
    ,'center',''  ...
    ,'radius',''  ...
    ,'z_bound',nan ...
    );
detector.type = 'circle';
detector.center = [0 0 2];
detector.radius = 0.5;
detector.z_bound = 2;
detector = orderfields(detector,detectorTemplate);

%% edges
edges.x = [-1 1];edges.y = [-1 1];edges.z = [0 1 2];

%% ihomogeneous scattering
mu_s = ones(numel(edges.x)-1,numel(edges.y)-1,numel(edges.z)-1)*5;
mu_s(1,1,2) = 20;
g = ones(numel(edges.x)-1,numel(edges.y)-1,numel(edges.z)-1)*0.1;

%% simulate
[opath,numTries,detections] = inhomogeneousScat_gNoZero_nonreflectiveBound_110(source,edges,mu_s,g,detector,pathCriteria);


