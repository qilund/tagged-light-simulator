function outputBool = isDetected(detector,c)
outputBool = false;
switch detector.type
    case 'circle'
        outputBool = sum((c - detector.center).^2) <= detector.radius^2;
        %         case 'square'
    case 'all'
        outputBool = true;
    case 'positive Z'
        outputBool = c(3) >= detector.z_bound;   
    otherwise
        error('Unimplemented detector type')
end

end
