function [opath,numTries,detections] = inhomogeneousScat_gNoZero_nonreflectiveBound(source,edges,mu_s,g,detector,pathCriteria)
%SINGLE_PHOTON Summary of this function goes here
%   Detailed explanation goes here

[c,k] = newPhoton(source);
curVoxCoord = [discretize(c(1),edges.x), ...
    discretize(c(2),edges.y), discretize(c(3),edges.z)];

visitedVoxels = false(size(mu_s));
visitedVoxels(curVoxCoord(1),curVoxCoord(2),curVoxCoord(3)) = true;
detBool = false;
numTries  =1;
path = zeros(1e5,3);
i = 1;
path(i,:) = c;
detections = 0;

while true
    i = i +1;
    [c,curVoxCoord,isAlive,visitedVoxels] = ...
        inhomogenous_step(c,k,mu_s,edges,curVoxCoord,visitedVoxels);
    if ~isAlive
        path(i,:) = c;
        i = i+1;
        opath = path(1:i-1,:);
        detBool = isDetected(detector,c);
		if detBool
			detections = detections+1;
		end
        if detBool & hasInteractedInhomogeneous(visitedVoxels,edges,pathCriteria) %#ok<AND2> 
            break
        else
            i = 1;
            [c,k] = newPhoton(source);
            curVoxCoord = [discretize(c(1),edges.x), ...
                discretize(c(2),edges.y), discretize(c(3),edges.z)];
            visitedVoxels = false(size(mu_s));
            visitedVoxels(curVoxCoord(1),curVoxCoord(2),curVoxCoord(3)) = true;
            numTries = numTries+1;
            path = zeros(1e5,3);
            path(1,:) = c;
            continue
        end
    end
    path(i,:) = c;
    g_in = g(curVoxCoord(1),curVoxCoord(2),curVoxCoord(3));
    k = scatterHenyeyGreenstein3Dhetero(g_in,k);

end
end
