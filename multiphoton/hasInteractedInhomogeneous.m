function outputBool = hasInteractedInhomogeneous(visitedVoxels,edges,pathCriteria)
switch pathCriteria.type
    case 'all'
        outputBool = true;
    case 'single voxel'
        c = pathCriteria.coordinate;
        targetVoxel = [discretize(c(1),edges.x), ...
        discretize(c(2),edges.y), discretize(c(3),edges.z)];
        outputBool = visitedVoxels(targetVoxel(1),targetVoxel(2),targetVoxel(3));
    otherwise
        error('this criteria is not implemented')
end
end
