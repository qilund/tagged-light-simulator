function [paths,Z,detections] = multiphoton(p)
% multiphoton generates photon paths in a scattering media defined by the
% input struct p. Recommendation: use SI units.
%
%
% Inputs:
% p.edges - edges of the medium struct with vectors x,y,z. Each edges
%		    vector includes at least 2 values, the outer edges of medium.
%	        Additional values indicate internal boundaries.
% p.mu_s		 - scattering rate, 1/m,
% p.g		     - scattering anisoropy (Henyey-Greenstein is used)
% p.n_internal	 - refractive index inside medium
% p.n_external	 - refractive index outside medium
% p.num_paths	 - number of requested paths to be generated.
% p.source		 - struct with source definitions
% p.detector	 - struct with detection definitions
% p.pathCriteria - struct with path discrimination
% p.homogeneous  - boolean which tells the simulator if it should see mu_s
%				  and g as a [Vx x Vy x Vz] sized matrix.
%
% Outputs:
% paths      -  a [num_paths x 1] sized cell matrix containing paths that
%				fulfill the pathCriteria
% Z			 - number of initialized photons
% detections - number of photons detected on the detector, disregarding
%			 pathCriteria

edges = p.edges;
mu_s = p.mu_s;
g = p.g;
n_internal  = p.n_internal;
n_external = p.n_external;

Z = cell(p.num_paths,1);
totalDetections = Z;
paths_cell = cell(p.num_paths,1);
paths = coder.nullcopy(paths_cell);







%% source - mandatory input
if ~isfield(p,'source')
	error('no source is defined!')
else
	source = p.source;
end
sourceTemplate = struct('type',''...
	,'center', [NaN NaN NaN]  ...
	,'radius', nan  ...
	,'start_k',[NaN NaN NaN] ...
	,'start_c',[NaN NaN NaN] ...
	);
source = addAndSort(source,sourceTemplate);

%% detector - mandatory input
if ~isfield(p,'detector')
	detector.type = 'all';
	warning('no detector specified')
else
	detector = p.detector;
end
detectorTemplate = struct('type',''...
	,'center',[NaN NaN NaN]  ...
	,'radius',nan  ...
	,'z_bound',nan...
	);
detector = addAndSort(detector,detectorTemplate);
%% path criteria - mandatory input
if ~isfield(p,'pathCriteria')
	pathCriteria.type = 'all';
else
	pathCriteria = p.pathCriteria;
end
pcTemplate = struct('type',''...
	,'radius',nan  ...
	,'coordinate',[NaN NaN NaN]  ...
	,'minZ',nan ...
	);
pathCriteria = addAndSort(pathCriteria,pcTemplate);
%% other


if ~isfield(p,'homogeneous')
	p.homogeneous = true;
end
%guessing that there will not be more than 5 different paramaters
simKey = num2str([any(g > 0,'all'), n_internal == n_external,p.homogeneous]);
simKey = erase(simKey,' ');

% if no progressbar setting input, assume no progressbar.
if ~isfield(p,'progressbar')
	p.progressbar = false;
end
if p.progressbar && ~isempty(ver('parallel'))
	if ~isfield(p,'progresstitle')
		p.progresstitle = 'Mulitphoton progression';
	end
	ppm = ParforProgressbar(p.num_paths,"title",p.progresstitle);
else
	ppm = dummyParforProgressbar();
end
%% switch to specialised MC simulator
switch simKey
	case '101'
		parfor i = 1:p.num_paths
			[path,tries,detections] = ...
				...homogeneousScat_gNoZero_reflectiveBound_101 ...
				mex101 ...
				(source,edges,mu_s,g,n_internal,n_external...
				,detector,pathCriteria);
			paths{i} = path;
			Z{i} = tries;
			totalDetections{i} = detections;
			ppm.increment();
		end
	case '001'
		parfor i = 1:p.num_paths
			[path,tries,detections] = homogeneousScat_gIsZero ...
				(source,edges,mu_s,n_internal,n_external,detector...
				,pathCriteria);
			paths{i} = path;
			Z{i} = tries;
			totalDetections{i} = detections;
			ppm.increment();
		end
	case '110'
		parfor i = 1:p.num_paths
			[path,tries,detections] = ...
				mex110 ...
				...inhomogeneousScat_gNoZero_nonreflectiveBound_110...
				(source,edges,mu_s,g,detector,pathCriteria);
			paths{i} = path;
			Z{i} = tries;
			totalDetections{i} = detections;
			ppm.increment();
		end
	case '010'
		parfor i = 1:p.num_paths
			[path,tries,detections] = inhomogeneousScat_gIsZero(source,edges,...
				mu_s,detector,pathCriteria);
			paths{i} = path;
			Z{i} = tries;
			totalDetections{i} = detections;
			ppm.increment();
		end
	case '111'
		parfor i = 1:p.num_paths
			[path,tries,detections] = ...
				... inhomogeneousScat_gNoZero_reflectiveBound...
				 mex111 ...
				(source,edges,mu_s,g,n_internal,n_external,detector,pathCriteria);
			paths{i} = path;
			Z{i} = tries;
			totalDetections{i} = detections;
			ppm.increment();
		end
	otherwise
		delete(ppm);
		error('case not yet implemented')
end
delete(ppm);
Z = cell2mat(Z);
Z = sum(Z);
totalDetections = cell2mat(totalDetections);
detections = sum(totalDetections);
end
function [opath,numTries,detections] = homogeneousScat_gIsZero(source,edges,mu_s,n_intr,n_extr,detector,pathCriteria)
%SINGLE_PHOTON Summary of this function goes here
%   Detailed explanation goes here

[c,k] = newPhoton(source);
numTries  =1;
path = zeros(1e4,3);
i = 1;
path(i,:) = c;
detections = 0;
while true
	i = i +1;
	if i >1e3
	end
	c = homogeneous_step(c,k,mu_s);
	[c,k,isAlive,boundaryEvent,refl_points] = reflectionTransmission3D(c,k,edges,n_intr,n_extr);
	if boundaryEvent
		path(i:i+size(refl_points,1)-1,:) = refl_points;
		i = i+size(refl_points,1);
	end
	if ~isAlive
		opath = path(1:i-1,:);
		detBool = isDetected(detector,c);
		if detBool
			detections = detections+1;
		end
		if detBool & hasInteracted(opath,pathCriteria) %#ok<AND2>
			break
		else
			i = 1;
			[c,k] = newPhoton(source);
			numTries = numTries+1;
			path = zeros(1e4,3);
		end
	end
	path(i,:) = c;
	k = scatterIsotropic3D(k);

end

end

%% help functions
function outputStruct = addAndSort(struct,template)
outputStruct = struct;
fns = fieldnames(struct);
fnt = fieldnames(template);
% List fields missing in s
missingIdx = find(~ismember(fnt,fns));
% Assign missing fields to s
for i = 1:length(missingIdx)
	outputStruct.(fnt{missingIdx(i)}) = template.(fnt{missingIdx(i)});
end
outputStruct = orderfields(outputStruct,template);
end


%% move this to its onw file to prepare mex file
function [opath,numTries,detections] = inhomogeneousScat_gIsZero(source,edges,mu_s,detector,pathCriteria)
%SINGLE_PHOTON Summary of this function goes here
%   Detailed explanation goes here

[c,k] = newPhoton(source);
curVoxCoord = [discretize(c(1),edges.x), ...
	discretize(c(2),edges.y), discretize(c(3),edges.z)];

detections = 0;
numTries  =1;
path = zeros(1e4,3);
i = 1;
path(i,:) = c;
while true
	i = i +1;
	[c,curVoxCoord,isAlive] = ...
		inhomogenous_step(c,k,mu_s,edges,curVoxCoord);
	if ~isAlive
		path(i,:) = c;
		i = i+1;
		opath = path(1:i-1,:);
		detBool = isDetected(detector,c);
		if detBool
			detections = detections+1;
		end
		if detBool & hasInteracted(opath,pathCriteria) %#ok<AND2>
			break
		else
			i = 1;
			[c,k] = newPhoton(source);
			curVoxCoord = [discretize(c(1),edges.x), ...
				discretize(c(2),edges.y), discretize(c(3),edges.z)];
			numTries = numTries+1;
			path = zeros(1e4,3);
		end
	end
	path(i,:) = c;
	k = scatterIsotropic3D(k);
end
end
