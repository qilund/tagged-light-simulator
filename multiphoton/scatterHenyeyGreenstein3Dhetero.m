function k = scatterHenyeyGreenstein3Dhetero(g,k)
% Scattering HenyeyGreenstein function
cosTheta= (1./(2.*g).*(1 + g.^2 - ...
    ((1 - g.^2)./(1- g + 2.*g.*rand)).^2));

sinTheta = sqrt(1 - cosTheta.^2);
phi = 2*pi*rand;
cosPhi = cos(phi);
sinPhi = sin(phi);
% I = indexes of photons NOT to close to having |k(:,3)| = 1;
if abs(k(3)) > 0.999
    k(1) = sinTheta*cosPhi;
    k(2) = sinTheta.*sinPhi;
    k(3) = sign(k(3)).*cosTheta;
else
    tx = k(1); % since I am using the current k(:,1) vector in the
    % calculation later;

    % k(:,1) update
    k(1) = sinTheta./sqrt(1-k(3).^2).* ...
        (k(1).*k(3).*cosPhi - k(2).*sinPhi) + ...
        k(1).*cosTheta;
    % k(:,2) update
    k(2) = sinTheta./sqrt(1-k(3).^2).* ...
        (k(2).*k(3).*cosPhi + tx.*sinPhi) + ...
        k(2).*cosTheta;
    % k(:,3) update
    k(3) = -sinTheta.*cosPhi.*sqrt(1-k(3).^2) + ...
        k(3).*cosTheta;
end
%shouldn't be necessary but this safeguards against numerical
%errors
norm = sqrt(k(1).^2+k(2).^2+k(3).^2);
k = k./norm;
%         clearvars tx cosTheta sinTheta cosPhi sinPhi
end
