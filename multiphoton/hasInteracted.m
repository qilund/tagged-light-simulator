function outputBool = hasInteracted(path,interaction)
switch interaction.type
    case 'all'
        outputBool = true;
    case 'sphere'
        %% Pseudocode
        % calculate kVectors
        % start point of each free path: a . end point: b. point of  = p
        % <u,v> denotes scalar product
        % v = (p-a)  - <(p-a),k>*k  is the min distance vector to the line defined
        % by a and b
        % minimum dist point on line: c = p + v
        %
        % 3 cases:  i)  c is inside free path length: dist = abs(v)
        %          ii)  c is before a on line:  dist = abs(a-p)
        %         iii)  c is after b on line:   dist = abs(b-p)
        %
        % for case i): add both nodes to interactNodes.
        % for case ii and iii): add the node that is close and the next and
        % previous nodes in fotPath to interactNodes.
        %
        %
        %
		%% allocation
		I1 = false(size(path,1),1);
		I2 = I1;
		I3 = I1;
		I4 = I1;
        %% real code
        %        b: j=1:J          a: j= 0:J-1
        k = path(2:end,:) - path(1:end-1,:);
        r_sqr = interaction.radius.^2;
        p = interaction.coordinate;
        % v = (a-p)                - <(a-p),k>*k
        v = (path(1:end-1,:) - p)  - sum((path(1:end-1,:)-p).*k,2).*k./sum(k.^2,2);

        c = p + v;
        kk = c-path(1:end-1,:);
        Ia = any(0>kk.*k,2);
        kk = c-path(2:end,:);
        Ib = any(0<kk.*k,2);
        Ia = (~Ia)&(~Ib)&(sum(v.^2,2)<r_sqr);
        % I2 = I2&sum((path(1:end-1,:)-US_r0.^2),2)<
		I1(1:end-1) = Ia;
		I2(2:end) = Ia;
        I1 = I1 | I2;
        I2 = sum( (path-p).^2,2)<r_sqr;
		I3(1:end-1) = I2(2:end);
		I4(2:end) = I2(1:end-1);
        I2 = I2 | I3 | I4;

        outputBool = any(I1|I2);
    case 'altitude Z'
        maxZ = max(path(:,3));
        outputBool = maxZ > interaction.minZ;
    otherwise
        error('this criteria is not implemented')
end
end
