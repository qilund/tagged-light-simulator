function [c,curVoxCoord,alive,visitedVoxels] = inhomogenous_step(c,k,mu_s,edges,curVoxCoord,visitedVoxels)
b = c; % b is copy of current coordinate vector c
stepTrack = 0; % bugfinding number
remLnXi = -log(rand); %sum(mu_s_i*s_i) = -ln(Xi) % i indexing different voxels
Imat = zeros(1,3,'int8');
alive = 1;

while remLnXi > 0
    stepTrack = stepTrack+1;
    maxStepCurrentVoxel = remLnXi./mu_s(curVoxCoord(1),curVoxCoord(2),curVoxCoord(3));

    if k(1)>=0
            b(1) = edges.x(curVoxCoord(1)+1);
    else
            b(1) = edges.x(curVoxCoord(1));
    end
    if k(2)>=0
            b(2) = edges.y(curVoxCoord(2)+1);
    else
            b(2) = edges.y(curVoxCoord(2));
    end
    if k(3)>=0
            b(3) = edges.z(curVoxCoord(3)+1);
    else
        b(3) = edges.z(curVoxCoord(3));
    end

    s = (abs((c-b)./k));
    [s,~] = min(s,[],2);
    %s is the step distance to the closest voxel. If s*mu_s_i
    %is larger than the remaining step length as if it had been
    %taken inside this voxel, then set s to the max step length if
    %all of it is in the current voxel.
    if s > maxStepCurrentVoxel
        s = maxStepCurrentVoxel;
        c = c+ k*s;
        return
    else
        remLnXi = remLnXi - s*mu_s(curVoxCoord(1),curVoxCoord(2),curVoxCoord(3));
        c = c+ k*s;
    end
    
    minE = [0 0 0];
    [minE(1),Imat(1)] = min((c(1)-edges.x).^2,[],2);
    [minE(2),Imat(2)] = min((c(2)-edges.y).^2,[],2);
    [minE(3),Imat(3)] = min((c(3)-edges.z).^2,[],2);
    [~,Iclosest] = min(minE);

    curVoxCoord(Iclosest) = curVoxCoord(Iclosest) - 2*((curVoxCoord(Iclosest)==Imat(Iclosest))-1/2);
    
    if any(curVoxCoord<1) | any(curVoxCoord>[numel(edges.x),numel(edges.y),numel(edges.z)]-1)
        %% terminate, reflection is implemented here.
        alive = 0;
        return
    end
    
    visitedVoxels(curVoxCoord(1),curVoxCoord(2),curVoxCoord(3)) = true;

end

end
