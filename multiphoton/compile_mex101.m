% COMPILE_MEX_101   Generate MEX-function mex101 from
%  homogeneousScat_gNoZero_reflectiveBound_101.
% 
% Script generated from project 'case101.prj' on 20-Apr-2022.
% 
% See also CODER, CODER.CONFIG, CODER.TYPEOF, CODEGEN.

%% Create configuration object of class 'coder.MexCodeConfig'.
cfg = coder.config('mex');
cfg.GenerateReport = true;
cfg.ReportPotentialDifferences = false;

%% Define argument types for entry-point
%  'homogeneousScat_gNoZero_reflectiveBound_101'.
ARGS = cell(1,1);
ARGS{1} = cell(8,1);
ARGS_1_1 = struct;
ARGS_1_1.type = coder.typeof('X',[1 100],[0 1]);
ARGS_1_1.center = coder.typeof(0,[1 3]);
ARGS_1_1.radius = coder.typeof(0);
ARGS_1_1.start_k = coder.typeof(0,[1 3]);
ARGS_1_1.start_c = coder.typeof(0,[1 3]);
ARGS{1}{1} = coder.typeof(ARGS_1_1);
ARGS_1_2 = struct;
ARGS_1_2.x = coder.typeof(0,[1 101],[0 1]);
ARGS_1_2.y = coder.typeof(0,[1 101],[0 1]);ARGS_1_2.z = coder.typeof(0,[1 101],[0 1]);
ARGS{1}{2} = coder.typeof(ARGS_1_2);
ARGS{1}{3} = coder.typeof(0);
ARGS{1}{4} = coder.typeof(0);
ARGS{1}{5} = coder.typeof(0);
ARGS{1}{6} = coder.typeof(0);
ARGS_1_7 = struct;
ARGS_1_7.type = coder.typeof('X',[1 100],[0 1]);
ARGS_1_7.center = coder.typeof(0,[1 3]);
ARGS_1_7.radius = coder.typeof(0);
ARGS_1_7.z_bound = coder.typeof(0);
ARGS{1}{7} = coder.typeof(ARGS_1_7);
ARGS_1_8 = struct;
ARGS_1_8.type = coder.typeof('X',[1 100],[0 1]);
ARGS_1_8.radius = coder.typeof(0);
ARGS_1_8.coordinate = coder.typeof(0,[1 3]);
ARGS_1_8.minZ = coder.typeof(0);
ARGS{1}{8} = coder.typeof(ARGS_1_8);

%% Invoke MATLAB Coder.
codegen -config cfg -o mex101 homogeneousScat_gNoZero_reflectiveBound_101 -args ARGS{1}

