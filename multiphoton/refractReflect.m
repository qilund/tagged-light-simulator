function [k_o,reflected] = refractReflect(k_i, n_next, n_prev, normal_vector);
% REFRACTREFLECT checks if a photon refracts or reflects over a boundary.
% reflection probability is determined using Fresnel equation (unpolarized 
% light), accounting for total internal reflection. Refraction determined
% using Snell's law


% theta is the angle between the normal vector and k_i
% dot product equals cosine, both vectors have unitary length, 
% cosTheta should be positive as angle is not larger than 90 degrees 
% (negative cosTheta only if normal vector "points opposite" to k_i).

sinThetaRefracted = 0; %defining here due to error in mex generation

cosTheta = (dot(k_i,normal_vector)); 
if cosTheta < 0
	error('logical error')
end

sinTheta = sqrt(1-cosTheta.^2); %sine Theta from trig identity

if sinTheta*n_prev > n_next%true if total internal reflection (sinTheta >0)
	reflected = true;
else
	%Snell's law
	sinThetaRefracted = n_prev/n_next*sinTheta;
    cosThetaRefracted = sqrt(1-(sinThetaRefracted).^2);

	% Fresnel equation for reflection probability/power reflection
    R = 1/2.*( ( (n_prev*cosTheta - n_next*cosThetaRefracted)./...
        (n_prev * cosTheta + n_next * cosThetaRefracted) ) .^2 ...
        + ((n_prev * cosThetaRefracted - n_next * cosTheta) ./...
        (n_prev * cosThetaRefracted + n_next * cosTheta) ) .^2 );
	
	reflected = rand<R;
end
if reflected
	%flip k over normal vector direction. since normal vectors ar allways
	%parallel to x y or z
	k_o = k_i;
	%normal vector is defined as either x,y or z unit vector.
	k_o(normal_vector~=0) = -k_i(normal_vector~=0);
else
	%rotate k_i around cross(normal_vector,k_i) that ThetaRefracted-Theta 
	rot = asin(sinThetaRefracted) - asin(sinTheta);
	% 
	% Rodrigues' formula
	u = cross(normal_vector,k_i);
	u = u/sqrt(sum(u.^2));
	k_o = k_i*cos(rot) + cross(u,k_i)*sin(rot);
end