% test101, test of case 101 function in multiphoton. 
%
% this script is used to generate the mex version of said case function
clearvars

%% define source with correct template
sourceTemplate = struct('type',''...
    ,'center',''  ...
    ,'radius',''  ...
    ,'start_k','' ...
    ,'start_c','' ...
    );

source.type = 'xy tophat';
source.center = [0 0.5 0];
source.radius = 0.5;
source.start_k = [0 0 1];
source.start_c = [0 0 0];
source = orderfields(source,sourceTemplate);


%% define path criteria with correct template
pcTemplate = struct('type',''...
    ,'radius',''  ...
    ,'coordinate',[nan nan nan]  ...
    ,'minZ',nan...
    );
pathCriteria.type = 'sphere';
pathCriteria.radius = 0.1;
pathCriteria.coordinate = [-0.7 -0.7 .2];
pathCriteria.minZ = 0;
pathCriteria = orderfields(pathCriteria,pcTemplate);

%% define detector with correct template
detectorTemplate = struct('type',''...
    ,'center',''  ...
    ,'radius',''  ...
    ,'z_bound',nan ...
    );
detector.type = 'circle';
detector.center = [0 0 2];
detector.radius = 0.5;
detector.z_bound = 1.99;
detector = orderfields(detector,detectorTemplate);
%% edges
edges.x = [-1 1];edges.y = [-1 1];edges.z = [0 2];

%% scattering
mu_s = 5;
g = 0.5;

%% refractive index (internal/external)
n_intr = 1.3;
n_extr = 1;

%% simulate
[opath,numTries,detections] = homogeneousScat_gNoZero_reflectiveBound_101(source,edges,mu_s,g,n_intr,n_extr,detector,pathCriteria);