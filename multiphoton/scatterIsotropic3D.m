function k = scatterIsotropic3D(k)
% isotropic: the photon can go in any direction after every
% scattering event. I.e, k is just randomized (but still unitary).
%(A fully random k in 3D was not as straight forward as I thought)
phi = 2*pi*rand;
tz = 2*rand - 1;%temporary variable
k(1) = sqrt(1-tz.^2).*cos(phi);
k(2) = sqrt(1-tz.^2).*sin(phi);
k(3) = tz;
end
