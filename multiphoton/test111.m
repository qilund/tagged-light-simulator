% test111, test of case 111 function in multiphoton. 
%
% this script is used to generate the mex version of said case function
clearvars

%% define source with correct template
sourceTemplate = struct('type',''...
    ,'center',''  ...
    ,'radius',''  ...
    ,'start_k','' ...
    ,'start_c','' ...
    );

source.type = 'point';
source.center = [-0.5 0 0];
source.radius = 0.5;
source.start_k = [0 0 1];
source.start_c = [-0.5 0 0];
source = orderfields(source,sourceTemplate);

%% define path criteria with correct template
pcTemplate = struct('type',''...
    ,'radius',''  ...
    ,'coordinate',[nan nan nan]  ...
    ,'minZ',nan...
    );
pathCriteria.type = 'all';
pathCriteria.radius = 0.5;
pathCriteria.coordinate = [0.8 0 1];
pathCriteria.minZ = 0;
pathCriteria = orderfields(pathCriteria,pcTemplate);

%% define detector with correct template
detectorTemplate = struct('type',''...
    ,'center',''  ...
    ,'radius',''  ...
    ,'z_bound',nan ...
    );
detector.type = 'circle';
detector.center = [0.5 0 2];
detector.radius = 0.5;
detector.z_bound = 2;
detector = orderfields(detector,detectorTemplate);

%% edges

edges.x = [-1 0 1];edges.y = [-1 1];edges.z = [0 2];

%% ihomogeneous scattering
mu_s = ones(numel(edges.x)-1,numel(edges.y)-1,numel(edges.z)-1)*5;
mu_s(2,1,1) = 10;

g = ones(numel(edges.x)-1,numel(edges.y)-1,numel(edges.z)-1)*0.5;

%% refractive index
n_extr = 1;
n_intr =  ones(numel(edges.x)-1,numel(edges.y)-1,numel(edges.z)-1)*1;
n_intr(2) = 2;

%% simulate
[opath,numTries,detections] = inhomogeneousScat_gNoZero_inhomogeneousRefractiveIndex_111(source,edges,mu_s,g,n_intr,n_extr,detector,pathCriteria);