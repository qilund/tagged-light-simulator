function [opath,numTries,detections] = homogeneousScat_gNoZero_reflectiveBound(source,edges,mu_s,g,n_intr,n_extr,detector,pathCriteria)
% homogeneousScat_gNoZero_allPaths

[c,k] = newPhoton(source);
numTries  =1;
path = zeros(1e5,3);
i = 1;
path(1,:) = c;
detections = 0;

while true
    i = i +1;
    c = homogeneous_step(c,k,mu_s);
    [c,k,isAlive,boundaryEvent,refl_points] = reflectionTransmission3D(c,k,edges,n_intr,n_extr);
    if boundaryEvent
        path(i:i+size(refl_points,1)-1,:) = refl_points;
        i = i+size(refl_points,1);
    end
    if ~isAlive
        opath = path(1:i-1,:);
		detBool = isDetected(detector,c);
		if detBool
			detections = detections+1;
		end
        if detBool & hasInteracted(opath,pathCriteria) %#ok<AND2> 
            break
        else
            i = 1;
            [c,k] = newPhoton(source);
            numTries = numTries+1;
            path = zeros(1e5,3);
            path(1,:) = c;
            continue
        end
    end
    path(i,:) = c;
    k = scatterHenyeyGreenstein3Dhetero(g,k);

end
end
