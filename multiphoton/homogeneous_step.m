function c = homogeneous_step(c,k,mu_s)
dS = -log(rand)/mu_s;
c = c + k.*dS;
end
