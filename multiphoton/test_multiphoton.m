clearvars
close all

p.num_paths = 20;
p.mu_s = 10;
p.g = 0.9;

p.n_internal = 1.0001;
p.n_external = 1;

p.detector.type = 'circle';
p.detector.center = [-1.5 0 0];
p.detector.radius = 0.5;
p.detector.z_bound = 6;

p.source.type = 'xy tophat';
p.source.center = [1.5 0 0];
p.source.start_c = [1.5 0 0];
p.source.radius = 0.5;
p.source.start_k = [0 0 1];

p.pathCriteria.type = 'all';
p.pathCriteria.radius = 0.5;
p.pathCriteria.coordinate = [-0.7 -0.7 .2];

p.homogeneous = true;



p.edges.x = [-4.5 4.5];p.edges.y = [-4.5 4.5];p.edges.z = [0, 5];
% tic
% mu_s =5;%ones(numel(p.edges.x)-1,numel(p.edges.y)-1,numel(p.edges.z)-1)*5;
% p.g = 0.7;%ones(numel(p.edges.x)-1,numel(p.edges.y)-1,numel(p.edges.z)-1)*0.5;
% mu_s(1,1,2) = 10; 
% p.mu_s = mu_s;
[paths,Z] = multiphoton(p);
% T = p.num_paths/Z
% paths = multiphoton_mex(p);
% toc
figure(2)
hold off
xf = zeros(p.num_paths,1);yf = xf;zf = xf;
x0 = xf; y0 = x0; z0=x0; 
for i = 1:p.num_paths
    figure(2)
    xf(i) = paths{i}(end,1);
    yf(i) = paths{i}(end,2);
    zf(i) = paths{i}(end,3);

    x0(i) = paths{i}(1,1);
    y0(i) = paths{i}(1,2);
    z0(i) = paths{i}(1,3);


end
plotpath(paths,p.edges);
hold on
plot3(xf,yf,zf,'.')
hold on
plot3(x0,y0,z0,'x')
% axis([-1 1 -1 1 0 2])

% phi = linspace(0,2*pi,20);
% theta = linspace(0,pi,10);
% [phi,theta] = ndgrid(phi,theta);
% x = p.pathCriteria.radius.*cos(phi).*sin(theta) + p.pathCriteria.coordinate(1);
% y = p.pathCriteria.radius.*sin(phi).*sin(theta) + p.pathCriteria.coordinate(2);
% z = p.pathCriteria.radius.*cos(theta) + p.pathCriteria.coordinate(3);
% plot3(x(:),y(:),z(:),'.k')

% set(gca,'ZDir','reverse')
% ax.YDir = 'reverse';
% zt = get(gca, 'ZTick');
% set(gca, 'ZTick',zt, 'ZTickLabel',fliplr(zt))


