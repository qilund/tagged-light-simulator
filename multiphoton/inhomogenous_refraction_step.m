function [c,curVoxCoord,alive,visitedVoxels,refl_points,boundaryEvent] = inhomogenous_refraction_step(c,k,mu_s,edges,curVoxCoord,visitedVoxels,n_internal,n_external)
b = c; % b is copy of current coordinate vector c
stepTrack = 0; % bugfinding number
remLnXi = -log(rand); %sum(mu_s_i*s_i) = -ln(Xi) % i indexing different voxels
Imat = zeros(1,3,'int8');
alive = true;
refl_points = zeros(10,3); %
i = 1; % index of path
while remLnXi > 0
	stepTrack = stepTrack+1;
	maxStepCurrentVoxel = remLnXi./mu_s(curVoxCoord(1),curVoxCoord(2),curVoxCoord(3));

	if k(1)>=0
		b(1) = edges.x(curVoxCoord(1)+1);
	else
		b(1) = edges.x(curVoxCoord(1));
	end
	if k(2)>=0
		b(2) = edges.y(curVoxCoord(2)+1);
	else
		b(2) = edges.y(curVoxCoord(2));
	end
	if k(3)>=0
		b(3) = edges.z(curVoxCoord(3)+1);
	else
		b(3) = edges.z(curVoxCoord(3));
	end

	s = (abs((c-b)./k));
	[s,~] = min(s,[],2);
	%s is the step distance to the closest voxel. If s*mu_s_i
	%is larger than the remaining step length as if it had been
	%taken inside this voxel, then set s to the max step length if
	%all of it is in the current voxel.
	if s > maxStepCurrentVoxel
		s = maxStepCurrentVoxel;
		c = c+ k*s;
		refl_points(i:end,:) = [];
		return
	else
		remLnXi = remLnXi - s*mu_s(curVoxCoord(1),curVoxCoord(2),curVoxCoord(3));
		c = c+ k*s;
	end

	minE = [0 0 0];
	[minE(1),Imat(1)] = min((c(1)-edges.x).^2,[],2);
	[minE(2),Imat(2)] = min((c(2)-edges.y).^2,[],2);
	[minE(3),Imat(3)] = min((c(3)-edges.z).^2,[],2);
	[~,Iclosest] = min(minE);
	prevVoxCoord = curVoxCoord;
	curVoxCoord(Iclosest) = curVoxCoord(Iclosest) - 2*((curVoxCoord(Iclosest)==Imat(Iclosest))-1/2);
	
	n_previous = n_internal(prevVoxCoord(1),prevVoxCoord(2),prevVoxCoord(3));

	%vector normal to the surface which is refracted over, points away from the
	%incident voxel, i.e. the angle between normal vector and k is < 90 deg
	normal_vector =  curVoxCoord-prevVoxCoord;

	%% check if the new position is outside medium.
	if any(curVoxCoord<1) | any(curVoxCoord>[numel(edges.x),numel(edges.y),numel(edges.z)]-1) %#ok<OR2>
		% new voxel is outside medium, unless there is a reflection- terminate
		[k,reflected] = refractReflect(k, n_external, n_previous,normal_vector);
		if ~reflected
			%not reflected - TERMINATE.
			refl_points(i:end,:) = [];
			alive = false;
			return
		end
		%there was a reflection, add point to reflected points
		refl_points(i,:) = c;
		i = i + 1;
		%since there was a reflection, current voxel is the previous one.
		curVoxCoord = prevVoxCoord;
	else
		% can now safely define n_current.
		n_current = n_internal(curVoxCoord(1),curVoxCoord(2),curVoxCoord(3));
		% check if the refractive index change between the two voxels is
		% significant (not using == operator to avoid numerical errors)
		if abs(n_previous - n_current) > 1e-5  % only refract if refractive index changes more than 1e-2;
			[k,reflected] = refractReflect(k, n_current, n_previous, normal_vector);
			
			%add point to reflected points (even if it is a refracted point)
			refl_points(i,:) = c;
			i = i + 1;

			% reverse current voxel if it was reflected.
			if reflected
				% if the photon is reflected, it doesn't actually leave the
				% current voxel.
				curVoxCoord = prevVoxCoord;
			end
		end

		visitedVoxels(curVoxCoord(1),curVoxCoord(2),curVoxCoord(3)) = true;
		
end

end
