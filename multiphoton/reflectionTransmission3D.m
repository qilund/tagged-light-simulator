function [c,k,survival,boundaryEvent,refl_points] = reflectionTransmission3D(c,k,edges,n_intr,n_extr)
% flow chart of this function
% 1. check indexes outside of c(:,1) boundary-
%   1a. back these packets via k to c(:,1) boundary and store "remaining
%   distance".
% 2. check indexes outside of c(:,2) boundary -
%   2a. back these packets via k to c(:,2) boundary and store "remaining
%   distance".
% 3. check indexes outside of c(:,3) boundary
%   3a. back these packets via k to c(:,3) boundary and store "remaining
%   distance".
% 4. check if any photon has left the domain. if no photon has
%   left, terminate function call (work is done)
% 5. reflect/transmit packets that were leaving domain in 1.
%   5a. reflected packets are moved their remaining distance in the
%   direction determined by reflection.
% 6. reflect/transmit packets that were leaving domain in 2.
%   6a. same as 5a but for photons leaving over c(:,2) boundary
% 7. reflect/transmit packets that were leaving domain in 3.
%   7a. same as 5a but for photons leaving over c(:,3) boundary
% 8. go to 1. to check so that no photon is is outside boundary
% after reflection (e.g. in corner).
%
% I have seen the program get stuck in an infinite loop here though
% I was not able to understand why. My best guess is that I
% encountered a numerical error. -DH 2020.

%assume survival
survival = true;
%assume 3 reflection points (multiple back and forth reflections
% may induce error)
refl_points = zeros(3,3);
% counter of number of reflections
i = 1;
%keeps track if there has been a boundary event
boundaryEvent = false;
while survival
    exitEvent = false;
    tx = c(1);
    ty = c(2);
    tz = c(3);
    %% x bounds
    % checking outside far x bound
    if (c(1) >= edges.x(end))
        % back up to far x boundary if any outside far x bound
        L = (c(1) - edges.x(end)) ./ (k(1));
        c(1) = edges.x(end);
        c(2) = c(2) - k(2).*L;
        c(3) = c(3) - k(3).*L;
        exitEvent = true;
    end

    if (c(1) <= edges.x(1))
        % back up to near boundary if any outside near bound
        L = (c(1) - edges.x(1)) ./ (k(1));%L>0 since k<0 and difference <0
        c(1) = edges.x(1);
        c(2) = c(2) - k(2).*L;
        c(3) = c(3) - k(3).*L;
        exitEvent = true;
    end

    %% y bounds
    if c(2) >= edges.y(end)
        L = (c(2) - edges.y(end)) ./ (k(2));
        c(1) = c(1) - k(1).*L;
        c(2) = edges.y(end);
        c(3) = c(3) - k(3).*L;
        exitEvent = true;
    end
    % back up to near boundary if any outside near bound
    if (c(2) <= edges.y(1))
        L = (c(2) - edges.y(1)) ./ k(2);
        c(1) = c(1) - k(1).*L;
        c(2) =  edges.y(1);
        c(3) = c(3) - k(3).*L;
        exitEvent = true;
    end
    %% z bounds
    if c(3) >= edges.z(end)
        L = (c(3) - edges.z(end)) ./ (k(3));
        c(1) = c(1) - k(1).*L;
        c(2) = c(2) - k(2).*L;
        c(3) = edges.z(end);
        exitEvent = true;
    end
    % back up to near boundary if any outside near bound
    if (c(3) <= edges.z(1))
        L = (c(3) - edges.z(1)) ./ k(3);
        c(1) = c(1) - k(1).*L;
        c(2) =  c(2) - k(2).*L;
        c(3) = edges.z(1);
        exitEvent = true;
    end
    %% check for exit event
    if ~exitEvent%no photons have been found outside medium
        refl_points(i:end,:) =[];
        return
    end
    boundaryEvent = true;
    refl_points(i,:) = c; % save reflection point
    i = i +1;
    %% reflect or terminate
    % if the photon has been found outside medium, we will now
    % reflect it and put it in its correct spot, or terminate it
    xi = rand;
    %z lower BOUND
    if (c(3) - edges.z(1)) == 0
        cosTheta = abs(k(3));
        R = internalReflection(cosTheta,n_intr,n_extr);
        if (xi<R)
            k(3) = -k(3);
            c(1) = tx;
            c(2) = ty;
            c(3) = 2*edges.z(1)-tz;
        else
            survival = false; %TERMINATED
        end
        continue
    end
    %X UPPER BOUND
    if (c(1) - edges.x(end)) == 0
        cosTheta = abs(k(1));
        R = internalReflection(cosTheta,n_intr,n_extr);
        if (xi<R)
            k(1) = -k(1);
            c(1) = 2*edges.x(end)-tx;
            c(2) = ty;
            c(3) = tz;
        else
            survival = false; %TERMINATED
        end
        continue
    end
    %X LOWER BOUND
    if (c(1) - edges.x(1)) == 0
        cosTheta = abs(k(1));
        R = internalReflection(cosTheta,n_intr,n_extr);
        if (xi<R)
            k(1) = -k(1);
            c(1) = 2*edges.x(1)-tx;
            c(2) = ty;
            c(3) = tz;
        else
            survival = false; %TERMINATED
        end
        continue
    end
    %Y UPPER BOUND
    if (c(2) - edges.y(end)) == 0
        cosTheta = abs(k(2));
        R = internalReflection(cosTheta,n_intr,n_extr);
        if (xi<R)
            k(2) = -k(2);
            c(1) = tx;
            c(2) = 2*edges.y(end)-ty;
            c(3) = tz;
        else
            survival = false; %TERMINATED
        end
        continue
    end
    % y lower bound
    if (c(2) - edges.y(1)) == 0
        cosTheta = abs(k(2));
        R = internalReflection(cosTheta,n_intr,n_extr);
        if (xi<R)
            k(2) = -k(2);
            c(1) = tx;
            c(2) = 2*edges.y(1)-ty;
            c(3) = tz;
        else
            survival = false; %TERMINATED
        end
        continue
    end
    %z upper bound
    if (c(3) - edges.z(end)) == 0
        cosTheta = abs(k(3));
        R = internalReflection(cosTheta,n_intr,n_extr);
        if (xi<R)
            k(3) = -k(3);
            c(1) = tx;
            c(2) = ty;
            c(3) = 2*edges.z(end)-tz;
        else
            survival = false; %TERMINATED
        end
        continue
    end

end
refl_points(i:end,:) =[];
end

