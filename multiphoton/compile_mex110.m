% COMPILE_MEX110   Generate MEX-function mex110 from
%  inhomogeneousScat_gNoZero_nonreflectiveBound_110.
% 
% Script generated from project
%  'inhomogeneousScat_gNoZero_nonreflectiveBound_110.prj' on 20-Apr-2022.
% 
% See also CODER, CODER.CONFIG, CODER.TYPEOF, CODEGEN.

%% Create configuration object of class 'coder.MexCodeConfig'.
cfg = coder.config('mex');
cfg.GenerateReport = true;
cfg.ReportPotentialDifferences = false;

%% Define argument types for entry-point
%  'inhomogeneousScat_gNoZero_nonreflectiveBound_110'.
ARGS = cell(1,1);
ARGS{1} = cell(6,1);
ARGS_1_1 = struct;
ARGS_1_1.type = coder.typeof('X',[1 100],[0 1]);
ARGS_1_1.center = coder.typeof(0,[1 3]);
ARGS_1_1.radius = coder.typeof(0);
ARGS_1_1.start_k = coder.typeof(0,[1 3]);
ARGS_1_1.start_c = coder.typeof(0,[1 3]);
ARGS{1}{1} = coder.typeof(ARGS_1_1);
ARGS_1_2 = struct;
ARGS_1_2.x = coder.typeof(0,[1 101],[0 1]);
ARGS_1_2.y = coder.typeof(0,[1 101],[0 1]);
ARGS_1_2.z = coder.typeof(0,[1 1001],[0 1]);
ARGS{1}{2} = coder.typeof(ARGS_1_2);
ARGS{1}{3} = coder.typeof(0,[100 100 100],[1 1 1]);
ARGS{1}{4} = coder.typeof(0,[100 100 100],[1 1 1]);
ARGS_1_5 = struct;
ARGS_1_5.type = coder.typeof('X',[1 100],[0 1]);
ARGS_1_5.center = coder.typeof(0,[1 3]);
ARGS_1_5.radius = coder.typeof(0);
ARGS_1_5.z_bound = coder.typeof(0);
ARGS{1}{5} = coder.typeof(ARGS_1_5);
ARGS_1_6 = struct;
ARGS_1_6.type = coder.typeof('X',[1 100],[0 1]);
ARGS_1_6.radius = coder.typeof(0);
ARGS_1_6.coordinate = coder.typeof(0,[1 3]);
ARGS_1_6.minZ = coder.typeof(0);
ARGS{1}{6} = coder.typeof(ARGS_1_6);

%% Invoke MATLAB Coder.
codegen -config cfg -o mex110 inhomogeneousScat_gNoZero_nonreflectiveBound_110 -args ARGS{1}

