function R = internalReflection(cTi,n_intr,n_extr)
%R = (RVert + RHor)/2 for unpolarized light
%Ir = indexes for total internal reflection
%       cTi = abs(k{c(:,1),c(:,2),c(:,3)}); %% cos of angle between normal of reflection
%       and k, eg (ex,k) = k(:,1) = cos Theta, the sign has no meaning so
%       remove by taking the abs value.

sT = sqrt(1-cTi.^2); %sine Theta
if sT^2*n_intr^2 > n_extr^2
    R = 1;% all total internal reflected photons have R = 1;
else
    cTt = sqrt(1-(n_intr/n_extr*sT).^2);

    R = 1/2.*( ( (n_intr*cTi - n_extr*cTt)./...
        (n_intr*cTi + n_extr*cTt) ).^2 ...
        + ((n_intr*cTt - n_extr* cTi) ./...
        (n_intr*cTt + n_extr* cTi)).^2 );
end
end
