classdef dummyParforProgressbar
	%Lazy way of turning on/off the parforProgressbar is to swap out the
	%progressbar for a dummy object 

	properties
		%has no properties
	end
	
	methods (Static)
		function increment()
			% increment on the dummy does nothing.
		end
	end
	methods
		function o = dummyParforProgressbar()
			%this object is just empty
		end

		function delete(~)
			%this object is just empty	
		end
	end
end