function [A1,Ac,D,V,M,Z,edges] = simulation_function(file_name)
if nargin == 0
	file_name = 'default_file_name';
end
if ~((ischar(file_name)) || (isstring(file_name)))
   error('Input ''file_name'' must be a string or a char')
end

mu_inc = 0.6*100;%1/m
mu_bcg = 0.2*100;%1/m

%% ultrasound field settings
us.type = 'simpleGaussianPulseZprop';

us.x0 = 0;
us.y0 = 0;
us.z0 = 2e-2;
us.sigma = 1.35e-3;
us.peak_pressure = 3e6;
us.frequency = 6e6;
us.interaction_distance = 0.8e-2;

%% light pulse settings
light.type = 'unitAreaGaussianPulse';
light.fwhm_power = 1e-6;

%% tagged light simulation settings
S = 2^10; %no unit
Q = 14;% no unit
Fs = 2*6*us.frequency;%Hz %Nyquist criteria met for first 6 sidebands, interest is the first sideband only
wl = 606e-9; %m


%% monte carlo settings
%medium properties
mc.g = 0.7; %
mc.mu_s = 5/(1-mc.g)*100;%m-1
mc.n_internal = 1.33;
mc.n_external = 1;

%medium size
edges.x = [-6 -0.25 0.25 6]*1e-2;
edges.y = [-6 -0.25 0.25 6]*1e-2;
edges.z = [0 1.75 2.25 10]*1e-2;

mc.edges = edges;
%source spatial distribution
mc.source.type = 'point';
mc.source.start_c = [-1.5 0 0]*1e-2;
mc.source.start_k = [0 0 1];

%detector area distribution
mc.detector.type = 'circle';
mc.detector.center = [1.5 0 0]*1e-2; % will change in simulation loop
mc.detector.radius = 0.5e-2;

% number of paths per MC simulation.
M = 50;%e3;
mc.num_paths = M;
%show progress
mc.progressbar = false;

%% initialize simulator
setupTaggedLightSimulator(us.type,light.type)
writeUltrasoundSettings(us)
writeLightSettings(light)

%% simulate paths
[paths,Z,~] = multiphoton(mc);

%% calculate D_matrix
[D_big,grid_map] = createDMatrix(paths,mc.edges,'full');

%% collapse D matrix to 2 voxels
I_us_voxel = all(grid_map == [us.x0,us.y0,us.z0],2);
D(:,1) = sum(D_big(:,~I_us_voxel),2);
D(:,2) =  D_big(:,I_us_voxel);
%% calculate spectra

y_vec = (-11:1:11)*1e-3;
N = numel(y_vec);
Ac = zeros(N,M);
A1 = zeros(N,M);
for i = 12;%1:numel(y_vec)
us.y0 = y_vec(i);
writeUltrasoundSettings(us)
[spectra,f] = tagged_spectra_simulator_smart(paths,S,Q,Fs,wl);
Ac(i,:) = signalSelection(f,spectra,0,us.frequency);
A1(i,:) = signalSelection(f,spectra,us.frequency,us.frequency);


end

%% calculate transmissions
mu = [mu_bcg; mu_inc];
V = 2;
F1 = A1*exp(-D*mu);
plot(y_vec,(F1+flipud(F1))/2)
close all

save(file_name,'-v7.3',"A1",'Ac','D','V','M','Z','edges','y_vec','mu','f','spectra');
%%
for i = 1:mc.num_paths
	figure
	plot(f/us.frequency,spectra(i,:),'LineWidth',4);
	set(gca,'XTick',[],'YTick',[]);
% 	myaxis = axis;
	axis([-3.5 3.5 0 2.2e-6])
	box off
	set(gca,'XColor', 'none','YColor','none')
	set(gca,'Position',[0.01 0.01 .98 .98])
% 	print([num2str(i) '.png'],'-dpng')

end
