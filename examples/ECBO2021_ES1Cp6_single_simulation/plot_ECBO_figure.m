
% plot_ECBO_figure

clearvars
close all
load('data_ECBO.mat')

n_mu=10;
mu_inc_vec = linspace(0.2,0.6,n_mu)*100;

mu_bcg = 0.2*100;%1/m

I = 2:2:22;
tmp = false(23,1);
tmp(I)=true;
A1 = A1(tmp,:);
y_vec = y_vec(tmp);

F_mat = zeros(numel(y_vec),n_mu);

for i = 1:n_mu
	mu_inc = mu_inc_vec(i);%1/m
	mu = [mu_bcg; mu_inc];
	V = 2;
	F= A1*exp(-D*mu);
	F = F + flipud(F);
	F_mat(:,i) = F;

end
F_mat = F_mat/max(F_mat,[],"all");


fh= figure;
fh.Units = 'centimeters';
fig_width = 12; % cm
set(fh,'Position',[fh.Position(1:2) fig_width fig_width/4/1.5*2]);
tiledlayout(1,2,'TileSpacing','Compact','Padding','Compact')
nexttile(1);
surf(y_vec*1000,mu_inc_vec/100,F_mat')
AZ = 148;
EL = 23;
view([AZ,EL])
F_line = F_mat(:,1);
tmp = mu_bcg*ones(numel(y_vec),1);
hold on
plot3(y_vec*1000,tmp/100,F_line,'LineWidth',2,'color','red')

nexttile(2);
F_mat = F_mat./F_line;

surf(y_vec*1000,mu_inc_vec/100,F_mat')
view([AZ,EL])