function simulation_function(file_name)
if nargin == 0
	file_name = 'default_file_name';
end
if ~((ischar(file_name)) || (isstring(file_name)))
   error('Input ''file_name'' must be a string or a char')
end

mu_inc = 0.4*100;%1/m
mu_bcg = 0.2*100;%1/m

%% ultrasound field settings
us.type = 'simpleGaussianPulseZprop';

us.x0 = 0;
us.y0 = 0;
us.z0 = 2e-2;
us.sigma = 3e-3;
us.peak_pressure = 1e6;
us.frequency = 3e6;
us.interaction_distance = 1e-2;

%% light pulse settings
light.type = 'unitAreaGaussianPulse';
light.fwhm_power = 1e-6;

%% tagged light simulation settings
S = 2^10; %no unit
Q = 15;% no unit
Fs = 2*6*us.frequency;%Hz %Nyquist criteria met for first 6 sidebands, interest is the first sideband only
wl = 606e-9; %m

%% monte carlo settings
%medium properties
mc.g = 0.7; %
mc.mu_s = 10/(1-mc.g)*100;%m-1
mc.n_internal = 1.33;
mc.n_external = 1;

%medium size
mc.edges.x = [-4 -0.5 0.5 4]*1e-2;
mc.edges.y = [-4 -0.5 0.5 4]*1e-2;
mc.edges.z = [0 1.5 2.5 4]*1e-2;%will change in simulation loop.

%source spatial distribution
mc.source.type = 'point';
mc.source.start_c = [-2 0 0]*1e-2;
mc.source.start_k = [0 0 1];

%detector area distribution
mc.detector.type = 'circle';
mc.detector.center = [2 0 0]*1e-2; % will change in simulation loop
mc.detector.radius = 0.5e-2;

% number of paths per MC simulation.
mc.num_paths = 10;
%show progress
mc.progressbar = false;

%% initialize simulator
setupTaggedLightSimulator(us.type,light.type)
writeUltrasoundSettings(us)
writeLightSettings(light)

%% simulate paths
[paths,Z,~] = multiphoton(mc);

%% calculate D_matrix
[D_big,grid_map] = createDMatrix(paths,mc.edges,'full');

%% collapse D matrix to 2 voxels
I_us_voxel = all(grid_map == [us.x0,us.y0,us.z0],2);
D(:,1) = sum(D_big(:,~I_us_voxel),2);
D(:,2) =  D_big(:,I_us_voxel);
%% calculate spectra
[spectra,f] = tagged_spectra_simulator_smart(paths,S,Q,Fs,wl);
Ac = signalSelection(f,spectra,0,us.frequency);
A1 = signalSelection(f,spectra,us.frequency,us.frequency);


%% calculate transmissions
mu = [mu_bcg; mu_inc];

T_carrier = Ac/Z*exp(-D*mu)
T_sideband = A1/Z*exp(-D*mu)

%%
for i = 1:mc.num_paths
	figure
	plot(f/us.frequency,spectra(i,:),'LineWidth',4);
	set(gca,'XTick',[],'YTick',[]);
	myaxis = axis;
% 	axis([-2.5 2.5 myaxis(3:4)])
	box off
	set(gca,'XColor', 'none','YColor','none')
	set(gca,'Position',[0.01 0.01 .98 .98])
	print([num2str(i) '.png'],'-dpng')

end
