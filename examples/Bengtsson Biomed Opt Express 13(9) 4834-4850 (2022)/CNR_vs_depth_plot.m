%Plotting Fig. 4 in "Comparison of contrast-to-noise ratios of different detection methods in ultrasound optical tomography"

%% Loading transmission data from MC + tagging simulator
clear all;
load("Photon_Transmissions.mat"); % Photon transmission for 1 cm^2 circulat input beam separated by 2.5 cm from 1 cm^2 circular detection point  

%% SETUP PARAMETERS  
N_0 = 1e15; % Nr of input photons per voxels
S_A = N_0*T_sideband_off_inclusion; % Number of tagged photons (+1st order) emitted across the 1 cm^2 detection area when probing tissue region A 
S_B = N_0*T_sideband_on_inclusion; % Number of tagged photons (+1st order) across the 1 cm^2 detection area when probing tissue region B 
C_A = N_0*T_carrier_off_inclusion; C_B = N_0*T_carrier_on_inclusion; % Untagged photons emitted across 1 cm^2 
As = pi*(1e2*0.5*600e-9)^2; % area of circular speckle with diameter 600 nm [cm^2]
S_A_sp = S_A*As; % Average number of tagged photons per speckle area at tissue output when probing tissue region A  
S_B_sp = S_B*As; % Average number of tagged photons per speckle area at tissue output when probing tissue region B 

n_H = 16; % Number of pixels per speckle - Off-axis holography
Npx_1 = 1e6; Npx_2 = 50e6; % Number of detector pixels for speckle contrast and off-axis holography
S = 1; % Detection area for spectralholeburning and photorefractive detection [cm^2]
T_u_1 = 1e-3; T_u_2 = 1e-8; % Transmission of untagged photons through filter 
eta_pr_1 = 0.10; eta_pr_2 = 0.35; % Photorefractive efficiency

eta_det_SHB = 0.15*0.5; % Detection efficiency spectral holeburning (15% detector QE and 50% loss due to detection NA) 
eta_det_H = 0.6*0.5; % Detection efficiency digital off-axis holography (60% detector QE and 50% loss due to polarizer)
eta_det_PR = 0.8*0.5; % Detection efficiency photorefractive crystals (80% detector QE and 50% loss due to detection NA)

%% CALCULATING CNR

% CNR for spectral holeburning 
CNR_SHB = sqrt(2*S*eta_det_SHB)*abs(S_A - S_B)./sqrt(S_A + S_B + T_u_1*C_A); % 25 kHz rep. rate, with 30 voxels, gives 200 pulses per voxel  
CNR_SHB2 = sqrt(2*S*eta_det_SHB)*abs(S_A - S_B)./sqrt(S_A + S_B + T_u_2*C_A);

% CNR for photorefractive detection 
CNR_PR = sqrt(2*S*eta_det_PR)*eta_pr_1*abs(S_A - S_B)./sqrt(C_A*(1+eta_pr_1).^2);
CNR_PR2 = sqrt(2*S*eta_det_PR)*eta_pr_2*abs(S_A - S_B)./sqrt(C_A*(1+eta_pr_2).^2);

% CNR for off-axis holography 
CNR_H = (sqrt(Npx_1)*eta_det_H*abs(S_A_sp - S_B_sp)./n_H)./sqrt(4*eta_det_H*(S_A_sp + S_B_sp)./n_H + 2);
CNR_H2 = (sqrt(Npx_2)*eta_det_H*abs(S_A_sp - S_B_sp)./n_H)./sqrt(4*eta_det_H*(S_A_sp + S_B_sp)./n_H + 2);

% CNR for speckle contrast
CNR_SC = sqrt(2*Npx_1)*abs(S_A - S_B)./C_A;
CNR_SC2 = sqrt(2*Npx_2)*abs(S_A - S_B)./C_A;

% figure(3);
% UOT_sig_det = 1e6*0.5*T_sideband_off_inclusion;
% semilogy(depths*1e2,UOT_sig_det)
% ylabel([char(181) 'W'])
% xlabel(['cm'])
%% PLOTTING 
close all
figure('units','inch','position',[0,0,3.8,3.8*0.8]); % size of figure
figure
% plot results spectral holeburning
semilogy(1e2*depths,CNR_SHB,'-','color', [10/255 100/255 170/255],'linewidth', 1.5); hold on;
semilogy(1e2*depths,CNR_SHB2,'--','color', [10/255 100/255 170/255],'linewidth', 1.5)
X = [1e2*depths', fliplr(1e2*depths')];
inBetween = [CNR_SHB', fliplr(CNR_SHB2')];
a = fill(X, inBetween,'g','LineStyle','none');
set(a,'Facealpha',0.4); set(a,'Facecolor', [10/255 100/255 170/255])

% plot results off-axis holography 
semilogy(1e2*depths,CNR_H,'-','color', [0/255 170/255 110/255],'linewidth', 1.5); hold on;
semilogy(1e2*depths,CNR_H2,'--','color', [0/255 170/255 110/255],'linewidth', 1.5)
inBetween = [CNR_H', fliplr(CNR_H2')];
a = fill(X, inBetween,'g','LineStyle','none');
set(a,'Facealpha',0.4); set(a,'Facecolor', [0/255 170/255 110/255])

% plot results photrefractive method 
semilogy(1e2*depths,CNR_PR,'-','color', [0/255 0/255 0/255],'linewidth', 1.5); hold on;
semilogy(1e2*depths,CNR_PR2,'--','color', [0/255 0/255 0/255],'linewidth', 1.5)
inBetween = [CNR_PR', fliplr(CNR_PR2')];
a = fill(X, inBetween,'g','LineStyle','none');
set(a,'Facealpha',0.35); set(a,'Facecolor', [0/255 0/255 0/255])

% plot results speckle contrast method 
semilogy(1e2*depths,CNR_SC,'-','color', [220/255 0/255 0/255],'linewidth', 1.5); hold on;
semilogy(1e2*depths,CNR_SC2,'--','color', [220/255 0/255 0/255],'linewidth', 1.5)
inBetween = [CNR_SC', fliplr(CNR_SC2')];
a = fill(X, inBetween,'g','LineStyle','none');
set(a,'Facealpha',0.35); set(a,'Facecolor', [220/255 0/255 0/255])

% plot CNR = 1 horizontal line
semilogy(1e2*depths,ones(1,length(1e2*depths)),':','color', [0/255 0/255 0/255],'linewidth', 1.5)

set( gca                       , ...
'FontSize' , 8);
set(gca, ...
  'Box'         , 'on'     , ...
  'TickDir'     , 'in'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'on'      , ...
  'YMinorTick'  , 'on'      , ...
  'XGrid'       , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'LineWidth'   , 1        );

xlabel('Imaging depth (cm)')
ylabel('Contrast-to-noise ratio')
set( gca                    , ...
    'FontName'   , 'Arial' );

axis([0.3 5 1e-1 2e3])