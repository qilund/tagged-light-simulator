%Plotting Fig. 5 in "Comparison of contrast-to-noise ratios of different detection methods in ultrasound optical tomography"

%% Loading transmission data from MC + tagging simulator
clearvars;
close all;
load("Photon_Transmissions_Vary_Absorption.mat"); % Photon transmission for 1 cm^2 circulat input beam separated by 2.5 cm from 1 cm^2 circular detection point  

K = size(mu_inc, 2); % number of different inclusion absorptions
for k = 1:1:K
    
    % SETUP PARAMETERS
    N_0 = 1e15; % Nr of input photons per voxels
    S_A = N_0*T_sideband_off_inclusion(k, :); % Number of tagged photons (+1st order) emitted across the 1 cm^2 detection area when probing tissue region A
    S_B = N_0*T_sideband_on_inclusion(k, :); % Number of tagged photons (+1st order) across the 1 cm^2 detection area when probing tissue region B
    C_A = N_0*T_carrier_off_inclusion(k, :); C_B = N_0*T_carrier_on_inclusion(k, :); % Untagged photons emitted across 1 cm^2
    As = pi*(1e2*0.5*600e-9)^2; % area of circular speckle with diameter 600 nm [cm^2]
    S_A_sp = S_A*As; % Average number of tagged photons per speckle area at tissue output when probing tissue region A
    S_B_sp = S_B*As; % Average number of tagged photons per speckle area at tissue output when probing tissue region B
    
    n_H = 16; % Number of pixels per speckle - Off-axis holography
    Npx_1 = 1e6; Npx_2 = 50e6; % Number of detector pixels for speckle contrast and off-axis holography
    S = 1; % Detection area for spectralholeburning and photorefractive detection [cm^2]
    T_u_1 = 1e-3; T_u_2 = 1e-8; % Transmission of untagged photons through filter
    eta_pr_1 = 0.10; eta_pr_2 = 0.35; % Photorefractive efficiency
    
    eta_det_SHB = 0.15*0.5; % Detection efficiency spectral holeburning (15% detector QE and 50% loss due to detection NA)
    eta_det_H = 0.6*0.5; % Detection efficiency digital off-axis holography (60% detector QE and 50% loss due to polarizer)
    eta_det_PR = 0.8*0.5; % Detection efficiency photorefractive crystals (80% detector QE and 50% loss due to detection NA)
    
    % CALCULATING CNR
    
    % CNR for spectral holeburning
    CNR_SHB(k, :) = sqrt(2*S*eta_det_SHB)*abs(S_A - S_B)./sqrt(S_A + S_B + T_u_1*C_A); % 25 kHz rep. rate, with 30 voxels, gives 200 pulses per voxel
    CNR_SHB2(k, :) = sqrt(2*S*eta_det_SHB)*abs(S_A - S_B)./sqrt(S_A + S_B + T_u_2*C_A);
    
    % CNR for photorefractive detection
    CNR_PR(k, :) = sqrt(2*S*eta_det_PR)*eta_pr_1*abs(S_A - S_B)./sqrt(C_A*(1+eta_pr_1).^2);
    CNR_PR2(k, :) = sqrt(2*S*eta_det_PR)*eta_pr_2*abs(S_A - S_B)./sqrt(C_A*(1+eta_pr_2).^2);
    
    % CNR for off-axis holography
    CNR_H(k, :) = (sqrt(Npx_1)*eta_det_H*abs(S_A_sp - S_B_sp)./n_H)./sqrt(4*eta_det_H*(S_A_sp + S_B_sp)./n_H + 2);
    CNR_H2(k, :) = (sqrt(Npx_2)*eta_det_H*abs(S_A_sp - S_B_sp)./n_H)./sqrt(4*eta_det_H*(S_A_sp + S_B_sp)./n_H + 2);
    
    % CNR for speckle contrast
    CNR_SC(k, :) = sqrt(2*Npx_1)*abs(S_A - S_B)./C_A;
    CNR_SC2(k, :) = sqrt(2*Npx_2)*abs(S_A - S_B)./C_A;

    
    %Photon numbers
    TPhoton_Nums_SHB(k, :) = 0.5*S_B; %50% loss due to detection NA
    CPhoton_Nums_SHB(k, :) = 0.5*T_u_1*C_B;
    TPhoton_Nums_SHB2(k, :) = 0.5*S_B;
    CPhoton_Nums_SHB2(k, :) = 0.5*T_u_2*C_B;
    
    TPhoton_Nums_PR(k, :) = 0.5*S_B; %50% loss due to detection NA
    CPhoton_Nums_PR(k, :) = 0.5*eta_pr_1*C_B;
    TPhoton_Nums_PR2(k, :) = 0.5*S_B;
    CPhoton_Nums_PR2(k, :) = 0.5*eta_pr_2*C_B;
    
    TPhoton_Nums_H(k, :) = 0.5*S_B; %50% loss due to polarizer
    CPhoton_Nums_H(k, :) = 0.5*eta_pr_1*C_B;
    TPhoton_Nums_H2(k, :) = 0.5*S_B;
    CPhoton_Nums_H2(k, :) = 0.5*eta_pr_2*C_B;
    
    TPhoton_Nums_SC(k, :) = S_B; 
    CPhoton_Nums_SC(k, :) = C_B;
    TPhoton_Nums_SC2(k, :) = S_B;
    CPhoton_Nums_SC2(k, :) = C_B;    
    
    
end

%Now have CNR for each absorption contrast; want where the CNR crosses the value of 1 for each contrast:
%loop through the depths, and find where the CNR = 1 
Cross_Points_SHB = zeros(K,1);
Cross_Points_PR = zeros(K,1);
Cross_Points_H = zeros(K,1);
Cross_Points_SC = zeros(K,1);

Cross_Points_SHB_2 = zeros(K,1);
Cross_Points_PR_2 = zeros(K,1);
Cross_Points_H_2 = zeros(K,1);
Cross_Points_SC_2 = zeros(K,1);

Cross_Points_PhotNum_SHB = zeros(K,1);
Cross_Points_PhotNum_PR = zeros(K,1);
Cross_Points_PhotNum_H = zeros(K,1);
Cross_Points_PhotNum_SC = zeros(K,1);

Cross_Points_PhotNum_SHB_2 = zeros(K,1);
Cross_Points_PhotNum_PR_2 = zeros(K,1);
Cross_Points_PhotNum_H_2 = zeros(K,1);
Cross_Points_PhotNum_SC_2 = zeros(K,1);

for k = 1:1:K
    

    %interpolate to the CNR data
    depths_interp = (0.3:0.001:6)*1e-2;
%     CNR_SHB_interp(k,:) = interp1(depths,CNR_SHB(k, :),depths_interp,'spline');

	CNR_SHB_interp = interp1(depths,CNR_SHB(k, :),depths_interp,'spline');
    CNR_PR_interp = interp1(depths,CNR_PR(k, :),depths_interp,'spline');
    CNR_H_interp = interp1(depths,CNR_H(k, :),depths_interp,'spline');
    CNR_SC_interp = interp1(depths,CNR_SC(k, :),depths_interp,'spline');

    CNR_SHB_2_interp = interp1(depths,CNR_SHB2(k, :),depths_interp,'spline');
    CNR_PR_2_interp = interp1(depths,CNR_PR2(k, :),depths_interp,'spline');
    CNR_H_2_interp = interp1(depths,CNR_H2(k, :),depths_interp,'spline');
    CNR_SC_2_interp = interp1(depths,CNR_SC2(k, :),depths_interp,'spline');

    TPhoton_SHB_interp = interp1(depths,TPhoton_Nums_SHB(k, :),depths_interp,'spline');
    TPhoton_PR_interp = interp1(depths,TPhoton_Nums_PR(k, :),depths_interp,'spline');
    TPhoton_H_interp = interp1(depths,TPhoton_Nums_H(k, :),depths_interp,'spline');
    TPhoton_SC_interp = interp1(depths,TPhoton_Nums_SC(k, :),depths_interp,'spline');

    TPhoton_SHB_2_interp = interp1(depths,TPhoton_Nums_SHB2(k, :),depths_interp,'spline');
    TPhoton_PR_2_interp = interp1(depths,TPhoton_Nums_PR2(k, :),depths_interp,'spline');
    TPhoton_H_2_interp = interp1(depths,TPhoton_Nums_H2(k, :),depths_interp,'spline');
    TPhoton_SC_2_interp = interp1(depths,TPhoton_Nums_SC2(k, :),depths_interp,'spline');
    
    


     %Figure for plotting the interpolated results versus non-interpolated.
     %(NB, k = 1 and k = 2 are zero, should plot from k = 3 onward if interested)
%     figure() 
%     semilogy( depths*1e2,  CNR_SHB(k, :), 'LineWidth',1.5, 'DisplayName', 'SHB '); hold on
%     semilogy( depths_interp*1e2,  CNR_SHB_interp, 'LineWidth',1.5, 'DisplayName', 'SHB interp'); hold on
%     
%     semilogy( depths*1e2,  CNR_PR(k, :), 'LineWidth',1.5, 'DisplayName', 'PR '); hold on
%     semilogy( depths_interp*1e2,  CNR_PR_interp, 'LineWidth',1.5, 'DisplayName', 'PR interp'); hold on
%     
%     semilogy( depths*1e2,  CNR_H(k, :), 'LineWidth',1.5, 'DisplayName', 'H '); hold on
%     semilogy( depths_interp*1e2,  CNR_H_interp, 'LineWidth',1.5, 'DisplayName', 'H interp'); hold on
%     
%     semilogy( depths*1e2,  CNR_SC(k, :), 'LineWidth',1.5, 'DisplayName', 'SC '); hold on
%     semilogy( depths_interp*1e2,  CNR_SC_interp, 'LineWidth',1.5, 'DisplayName', 'SC interp'); hold on
%     legend('-DynamicLegend', 'Location', 'northeast');
%     
%     grid on
%     xlabel('Depth (cm)')
%     ylabel('CNR', 'interpreter', 'latex')
%     title(['Interp vs non interp (params 1), loop ' num2str(k) ' of a total ' num2str(K)])


    
    for n = 1:1:size(depths_interp, 2)-1

        %% Params 1
        if( CNR_SHB_interp(n)-1 > 0 && CNR_SHB_interp(n+1)-1 < 0 )
            disp(['Depth found: ' num2str(100*(depths_interp(n) + depths_interp(n+1))/2)])
            Cross_Points_SHB(k) = 100*(depths_interp(n) + depths_interp(n+1))/2;
                      
            Cross_Points_PhotNum_SHB(k) = (TPhoton_SHB_interp( n) + TPhoton_SHB_interp( n+1))/2;
        end
        if( CNR_PR_interp(n)-1 > 0 && CNR_PR_interp(n+1)-1 < 0 )
            disp(['Depth found: ' num2str(100*(depths_interp(n) + depths_interp(n+1))/2)])
            Cross_Points_PR(k) = 100*(depths_interp(n) + depths_interp(n+1))/2;
            
            Cross_Points_PhotNum_PR(k) = (TPhoton_PR_interp( n) + TPhoton_PR_interp( n+1))/2;
        end
        if( CNR_H_interp(n)-1 > 0 && CNR_H_interp(n+1)-1 < 0 )
            disp(['Depth found: ' num2str(100*(depths_interp(n) + depths_interp(n+1))/2)])
            Cross_Points_H(k) = 100*(depths_interp(n) + depths_interp(n+1))/2;
            
            Cross_Points_PhotNum_H(k) = (TPhoton_H_interp( n) + TPhoton_H_interp( n+1))/2;
        end
        if( CNR_SC_interp(n)-1 > 0 && CNR_SC_interp(n+1)-1 < 0 )
            disp(['Depth found: ' num2str(100*(depths_interp(n) + depths_interp(n+1))/2)])
            Cross_Points_SC(k) = 100*(depths_interp(n) + depths_interp(n+1))/2;
            
            Cross_Points_PhotNum_SC(k) = (TPhoton_SC_interp( n) + TPhoton_SC_interp( n+1))/2;
        end
        %% Params 2
        if( CNR_SHB_2_interp(n)-1 > 0 && CNR_SHB_2_interp(n+1)-1 < 0 )
            disp(['Depth found: ' num2str(100*(depths_interp(n) + depths_interp(n+1))/2)])
            Cross_Points_SHB_2(k) = 100*(depths_interp(n) + depths_interp(n+1))/2;
            
            Cross_Points_PhotNum_SHB_2(k) = (TPhoton_SHB_2_interp( n) + TPhoton_SHB_2_interp( n+1))/2;
        end
        if( CNR_PR_2_interp(n)-1 > 0 && CNR_PR_2_interp(n+1)-1 < 0 )
            disp(['Depth found: ' num2str(100*(depths_interp(n) + depths_interp(n+1))/2)])
            Cross_Points_PR_2(k) = 100*(depths_interp(n) + depths_interp(n+1))/2;
            
             Cross_Points_PhotNum_PR_2(k) = (TPhoton_PR_2_interp( n) + TPhoton_PR_2_interp( n+1))/2;
        end
        if( CNR_H_2_interp(n)-1 > 0 && CNR_H_2_interp(n+1)-1 < 0 )
            disp(['Depth found: ' num2str(100*(depths_interp(n) + depths_interp(n+1))/2)])
            Cross_Points_H_2(k) = 100*(depths_interp(n) + depths_interp(n+1))/2;
            
            Cross_Points_PhotNum_H_2(k) = (TPhoton_H_2_interp( n) + TPhoton_H_2_interp( n+1))/2;
        end
        if( CNR_SC_2_interp(n)-1 > 0 && CNR_SC_2_interp(n+1)-1 < 0 )
            disp(['Depth found: ' num2str(100*(depths_interp(n) + depths_interp(n+1))/2)])
            Cross_Points_SC_2(k) = 100*(depths_interp(n) + depths_interp(n+1))/2;
            
            Cross_Points_PhotNum_SC_2(k) = (TPhoton_SC_2_interp( n) + TPhoton_SC_2_interp( n+1))/2;
        end
    end
    
end





Contrast =  mu_inc./mu_bcg;


figure()
%% Plot SHB
plot(Contrast ,Cross_Points_PhotNum_SHB,'-','color', [10/255 100/255 170/255],'linewidth', 1.5, 'DisplayName', 'SHB T_U = -30 dB'); hold on
plot(Contrast ,Cross_Points_PhotNum_SHB_2,'--','color', [10/255 100/255 170/255],'linewidth', 1.5, 'DisplayName', 'SHB T_U = -80 dB'); hold on


%% Plot PR
plot(Contrast ,Cross_Points_PhotNum_PR,'-','color', [0/255 0/255 0/255],'linewidth', 1.5, 'DisplayName', 'PR \eta = 0.1'); hold on
plot(Contrast ,Cross_Points_PhotNum_PR_2,'--','color', [0/255 0/255 0/255],'linewidth', 1.5, 'DisplayName', 'PR \eta = 0.35'); hold on

%% Plot H
plot(Contrast ,Cross_Points_PhotNum_H,'-','color', [0/255 170/255 110/255],'linewidth', 1.5, 'DisplayName', 'Holo 1 Mpx'); hold on
plot(Contrast ,Cross_Points_PhotNum_H_2,'--','color', [0/255 170/255 110/255],'linewidth', 1.5, 'DisplayName', 'Holo 50 MPx'); hold on

%% Plot SC
plot(Contrast ,Cross_Points_PhotNum_SC,'-','color', [220/255 0/255 0/255],'linewidth', 1.5, 'DisplayName', 'Speckle Contrast 1 Mpx'); hold on
plot(Contrast ,Cross_Points_PhotNum_SC_2,'--','color', [220/255 0/255 0/255],'linewidth', 1.5, 'DisplayName', 'Speckle Contrast 50 Mpx'); hold on

legend('-DynamicLegend', 'Location', 'bestoutside');
grid on
xlabel('Absortpion contrast (inclusion/background)')
ylabel('Photon Number', 'interpreter', 'latex')
title('CNR = 1 limit as a function of Absorption Contrast')




%% Plot the results
figure()
%% Plot SHB
plot(Contrast ,Cross_Points_SHB,'-','color', [10/255 100/255 170/255],'linewidth', 1.5, 'DisplayName', 'SHB T_U = -30 dB'); hold on
plot(Contrast ,Cross_Points_SHB_2,'--','color', [10/255 100/255 170/255],'linewidth', 1.5, 'DisplayName', 'SHB T_U = -80 dB'); hold on

X = [Contrast, fliplr(Contrast)];
inBetween = [(Cross_Points_SHB)', fliplr(Cross_Points_SHB_2')];
a = fill((X), (inBetween),'g','LineStyle','none', 'HandleVisibility', 'off');
set(a,'Facealpha',0.4); set(a,'Facecolor', [10/255 100/255 170/255])


%% Plot PR
plot(Contrast ,Cross_Points_PR,'-','color', [0/255 0/255 0/255],'linewidth', 1.5, 'DisplayName', 'PR \eta = 0.1'); hold on
plot(Contrast ,Cross_Points_PR_2,'--','color', [0/255 0/255 0/255],'linewidth', 1.5, 'DisplayName', 'PR \eta = 0.35'); hold on

X = [Contrast, fliplr(Contrast)];
inBetween = [Cross_Points_PR', fliplr(Cross_Points_PR_2')];
a = fill(X, inBetween,'g','LineStyle','none', 'HandleVisibility', 'off');
set(a,'Facealpha',0.4); set(a,'Facecolor', [0/255 0/255 0/255])

%% Plot H
plot(Contrast ,Cross_Points_H,'-','color', [0/255 170/255 110/255],'linewidth', 1.5, 'DisplayName', 'Holo 1 Mpx'); hold on
plot(Contrast ,Cross_Points_H_2,'--','color', [0/255 170/255 110/255],'linewidth', 1.5, 'DisplayName', 'Holo 50 MPx'); hold on

X = [Contrast, fliplr(Contrast)];
inBetween = [Cross_Points_H', fliplr(Cross_Points_H_2')];
a = fill(X, inBetween,'g','LineStyle','none', 'HandleVisibility', 'off');
set(a,'Facealpha',0.4); set(a,'Facecolor', [0/255 170/255 110/255])

%% Plot SC
plot(Contrast ,Cross_Points_SC,'-','color', [220/255 0/255 0/255],'linewidth', 1.5, 'DisplayName', 'Speckle Contrast 1 Mpx'); hold on
plot(Contrast ,Cross_Points_SC_2,'--','color', [220/255 0/255 0/255],'linewidth', 1.5, 'DisplayName', 'Speckle Contrast 50 Mpx'); hold on

X = [Contrast, fliplr(Contrast)];
inBetween = [Cross_Points_SC', fliplr(Cross_Points_SC_2')];
a = fill(X, inBetween,'g','LineStyle','none', 'HandleVisibility', 'off');
set(a,'Facealpha',0.35); set(a,'Facecolor', [220/255 0/255 0/255])

legend('-DynamicLegend', 'Location', 'bestoutside');
set( gca                       , ...
'FontSize' , 8);
set(gca, ...
  'Box'         , 'on'     , ...
  'TickDir'     , 'in'     , ...
  'TickLength'  , [.01 .01] , ...
  'XMinorTick'  , 'on'      , ...
  'YMinorTick'  , 'on'      , ...
  'XGrid'       , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'LineWidth'   , 1        );

xlabel('Absortpion contrast (inclusion/background)')
ylabel('Depth where CNR = 1 (cm)')
set( gca                    , ...
    'FontName'   , 'Arial' );
grid on
axis([0.98 2.5 0 5.5])

function disp(~)
	return
end