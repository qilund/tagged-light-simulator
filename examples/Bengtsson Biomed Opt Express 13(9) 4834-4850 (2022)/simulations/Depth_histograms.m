close all
%first, load ABCD
D_tot = full(sum(D,2));
for i = 1:20
Idx = full(I1(i,:)>0);
figure
histogram(D_tot(Idx))
mean_dist(i) = mean(D_tot(Idx));
std_dist(i) = std(D_tot(Idx));
T(i) = sum(Idx)/20e6;
mytitle = sprintf('Msig = %i, T = %f, z = %i',sum(Idx),T(i),map(2*i,3)*1e3);
title(mytitle)
print(['histograms\' mytitle '.png'],'-dpng','-r400')
end
depth = map(2:2:40,3);
figure
errorbar(depth*100,mean_dist,std_dist,'LineWidth',2)
hold on
plot(depth*100,depth, 'LineWidth',2);
xlabel('US depth [cm]')
ylabel('Path length [m]')
figure
plot(depth*100,T, 'LineWidth',2);
xlabel('US depth [cm]')
ylabel('Transmission tagged photons')
%path length should follow this distribution (distance is x):
%y =  y0 + a*exp(-log(x/lc).^2./(2*w.^2))