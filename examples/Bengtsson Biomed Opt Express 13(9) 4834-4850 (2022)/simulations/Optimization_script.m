% Optimiziation script 
%load('ABCD.mat')
close all
controlled_modes = 1000;
mu_a = 20; %m^-1
D_hom = full(sum(D,2))';
for i = 1:size(I1,1)

	T_abs = I1(i,:).*exp(-mu_a*D_hom)/Z;
	T_no_abs =I1(i,:)/Z;
	[I_sort,Idx] = sort(T_abs,'descend');
	T_opt(i) = mean(T_abs(Idx(1:10)))*controlled_modes;
	T_nrml(i) = sum(T_abs);
% 	figure;
% 	H=histogram(T_abs(T_abs>0));
% 	title('%i, abs',i)

% 	
% 	cur_ax = axis;
% 	axis([cur_ax(1:2) 0 H.Values(2)])
% 	drawnow
% 	
% 		figure;
% 	H=histogram(T_no_abs(T_no_abs>0));
% 	title('%i, no abs',i)

end
figure;
z = unique(map(2:end,3));
semilogy(z,T_opt)
hold on
plot(z,T_nrml)