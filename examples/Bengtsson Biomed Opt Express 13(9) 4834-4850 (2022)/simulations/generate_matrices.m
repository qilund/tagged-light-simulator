function generate_matrices(data_file_name)


% load settings.
s = data_gen_settings;
%end_i: how many sub iterations are required to simulate all paths.
end_i = s.num_paths/s.paths_per_iteration;

%setting up the tagged Ligh simulator for used ultrasound and light pulses
setupTaggedLightSimulator(s.ultrasound_case,s.light_case);
% write the light settings file.
writeLightSettings(s.light);

% variable breakout to ease reading
us = s.ultrasound;
us_freq = us.frequency;
p = s.p; % p is the settings for the MC calculation (multiphoton)
p.num_paths = s.paths_per_iteration/2;

% calculate spectra of noninteracting paths.
[spectra,f] = noInteractSpectra(s.S,s.Fs);
I1_min = signalSelection(f,spectra,us_freq,us_freq);
IC_max = signalSelection(f,spectra,0,us_freq);
%% create the voxel map for the simulation
[~,map] = createDMatrix([],p.edges,'full');

IxM = max(map(:,1)) == map(:,1);
Ixm = min(map(:,1)) == map(:,1);
IyM = max(map(:,2)) == map(:,2);
Iym = min(map(:,2)) == map(:,2);
IzM = max(map(:,3)) == map(:,3);
Izm= min(map(:,3)) == map(:,3);
Ibcg = Ixm | IxM | Iym | IyM | Izm | IzM;
y_middle = unique(map(:,2));
y_middle = y_middle(3);
Ibcg = Ibcg | (y_middle == map(:,2));

map(Ibcg,:) = [];
map = [nan nan nan;map];

%% initialize variables or reload from previously stopped simulation

tmpD = zeros(s.paths_per_iteration,size(map,1));
try
	load(data_file_name)
	start_i = i+1;
catch
	start_i = 1;
	D = sparse(zeros(s.num_paths,size(map,1)));
% 	I1 = cell(numel(us_y_pos),1);
	I1 = sparse(zeros(numel(s.probed_depths),s.num_paths));
	Ic = I1;
	Z = 0;
end
for i = start_i:end_i
	[tmpPaths,newZ,~] = multiphoton(p);
	paths = [tmpPaths;cellfun(@mirrorPathsOverY0,tmpPaths,'UniformOutput',false)];
	Z = Z + newZ*2;
	[newD,~] = createDMatrix(paths,p.edges,'sparse');

	tmpD(:,1) = sum(newD(:,Ibcg),2);
	tmpD(:,2:end) = newD(:,~Ibcg);
	D(1+(i-1)*s.paths_per_iteration:i*s.paths_per_iteration,:) = tmpD;
	for k = 1:numel(s.probed_depths)
		us.z0 = s.probed_depths(k);
		trycatch = true;
		while trycatch
			try
				writeUltrasoundSettings(us)
				trycatch = false;
			catch
				pause(5);
			end
		end
		[spectra,f] = tagged_spectra_simulator_smart(paths,s.S,s.Q,s.Fs,s.wl);

		tmpI1 = signalSelection(f,spectra,us_freq,us_freq);
		tmpIc = signalSelection(f,spectra,0,us_freq);
        

        no_interact = tmpI1==I1_min;
		tmpI1(no_interact) = 0; 
        tmpIc(no_interact) = 1; % since pulse energy energy is 1, no interction should give 1 in carrier. This is useful to stor Ic as sparse;
		I1(k,1+(i-1)*s.paths_per_iteration:i*s.paths_per_iteration) = tmpI1;
		Ic(k,1+(i-1)*s.paths_per_iteration:i*s.paths_per_iteration) = tmpIc-1;

	end
	trycatch = true;
	while trycatch
		try
			save(data_file_name,"i","I1","Ic","D",'map','Z')
			trycatch = false;
		catch
			pause(5);
		end
	end
end
end

function P = mirrorPathsOverY0(P)
P(:,2)=-P(:,2);
end