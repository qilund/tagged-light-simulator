% main_script. With unmodified settings this took approx 24 hrs on the
% machine with the following specs:
% CPU: AMD Ryzen Threadripper 3960X 24-Core Processor    3.79 GHz 
% RAM: 128 GB running at 3600 MHz
%
% The time can be roughly halved by running lines 10 and 11 on a second
% instance of matlab (the two instances must have separate TaggingFunction
% directories on its path, i.e. one of the instances must run with a copy)

generate_matrices('A')
generate_matrices('B')
generate_matrices('C')
generate_matrices('D')
supermerge
preliminary_plot_script
disp('simulation done')