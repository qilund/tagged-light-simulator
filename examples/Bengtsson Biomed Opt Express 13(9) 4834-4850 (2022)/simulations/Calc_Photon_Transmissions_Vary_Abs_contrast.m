%For varying the inclusion absoprtion 
clearvars;
load ABCD.mat

N = size(I1,1); % number of probed positions
V = size(D,2); % number of voxels

depths = unique(map(2:end,3)); % probed depths (map contains all voxel centers
y_us = -2.5e-3;
y_inclusion = 2.5e-3;

mu_bcg = 20;%m-1;
mu_inc = 20:0.1:70;%

K = size(mu_inc, 2); % number of different inclusion absorptions

T_sideband_on_inclusion = zeros(K,N);
T_sideband_off_inclusion= zeros(K,N);

T_carrier_on_inclusion  = zeros(K,N);
T_carrier_off_inclusion = zeros(K,N);

for k = 1:1:K %sweep over number of inclusion absorptions
    
    disp(['Background abs number: ' num2str(k) ' of a total ' num2str(K)])
    for n = 1:N %sweep over number of US depths
        Index_ultrasound_voxel = all(map == [0 y_us depths(n)],2);
        Index_other_voxel = all(map == [0 y_inclusion depths(n)],2);
        
        
        mu = ones(V,1)*mu_bcg;
        mu(Index_ultrasound_voxel) = mu_inc(k);
        T_sideband_on_inclusion(k, n) = (I1(n,:)*exp(-D*mu))/Z;
        T_carrier_on_inclusion(k, n) = ((Ic(n,:)+1)*exp(-D*mu))/Z;
        
        mu = ones(V,1)*mu_bcg;
        mu(Index_other_voxel) = mu_inc(k);
        T_sideband_off_inclusion(k, n) = (I1(n,:)*exp(-D*mu))/Z;
        T_carrier_off_inclusion(k, n) = ((Ic(n,:)+1)*exp(-D*mu))/Z;
        
    end
    
end

figure
hold on
for k = 1:1:K
    semilogy(depths,T_sideband_off_inclusion(k, :),depths,T_sideband_on_inclusion(k, :))
end
xlabel('inclusion positiuon')
ylabel('Signal transmission')
set(gca,'YScale','log')

figure
for k = 1:1:K
    semilogy(depths,T_carrier_off_inclusion(k, :),depths,T_carrier_on_inclusion(k, :))
end
xlabel('inclusion positiuon')
ylabel('Signal transmission')
set(gca,'YScale','log')

save('Photon_Transmissions_Vary_Absorption','T_carrier_off_inclusion','T_carrier_on_inclusion','T_sideband_on_inclusion','T_sideband_off_inclusion','depths', 'mu_bcg', 'mu_inc')


