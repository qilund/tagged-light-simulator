function varargout = data_gen_settings
% all settings for the simulation. Running it without output plots the
% ultrasound, medium with voxels and probe positions and the light pulse.
%% Calculation settings
s.num_paths = 5e6;
s.paths_per_iteration = 1e4;
s.displayProgress=0;
s.S = 2^7;% time vector size
s.Q = 10; % number of segmentations per free path length
s.Fs = 3e6*2*3.5;%Hz

% s.p.num_paths = s.paths_per_iteration;
s.p.pathCriteria.type = 'all';

%% medium optical properties
s.p.mu_s = 50*100;%m-1
s.p.g = 0.9; %
s.p.n_internal = 1.3;
s.p.n_external = 1;

%% medium size
s.x_width = 12/100;
s.y_width = 12/100;
s.z_width = 10/100;



%% light source

s.p.source.type = 'xy tophat';
s.p.source.center =  [-2.5/2 0 0]*1e-2;
s.p.source.radius =   0.5642e-2;
s.p.source.start_k = [0 0 1];

%% detector
s.p.detector.type = 'circle';
s.p.detector.center = [2.5/2 0 0]*1e-2;
s.p.detector.radius = 0.5642e-2;
% z position of source is allways 0

%% voxel dimesions

sep_X = 0.3e-2;
sep_Y = 0.3e-2;
sep_Z = 0.3e-2;
inc_sep = [5e-3,5e-3, 5e-3]; % needs to be equal or larger than sep_Y
s.voxel_edges.x = [-s.x_width/2,-sep_X/2,sep_X/2,s.x_width/2];
s.voxel_edges.y = unique([-s.y_width/2, ...
	-inc_sep(3)/2-sep_Y/2,-inc_sep(3)/2+sep_Y/2,...
	-inc_sep(2)/2-sep_Y/2,-inc_sep(2)/2+sep_Y/2,...
	-inc_sep(1)/2-sep_Y/2,-inc_sep(1)/2+sep_Y/2,...
	inc_sep(1)/2-sep_Y/2,inc_sep(1)/2+sep_Y/2, ...
	inc_sep(2)/2-sep_Y/2,inc_sep(2)/2+sep_Y/2, ...
	inc_sep(3)/2-sep_Y/2,inc_sep(3)/2+sep_Y/2, ...
	s.y_width/2]);

s.probed_depths = (sep_Z:sep_Z:6*1e-2);
tmp = [s.probed_depths(1)-sep_Z/2, (s.probed_depths + sep_Z/2) ];
s.voxel_edges.z = [0, tmp, s.z_width];


%% ultrasound pulse properties;
s.ultrasound_case = 'simpleGaussianPulseZprop';
s.ultrasound.peak_pressure = 2e6; %Pa
s.ultrasound.frequency = 3e6; %Hz
s.ultrasound.sigma = 1.35e-3;
s.ultrasound.x0 = 0;
s.ultrasound.y0 = unique(-inc_sep/2);
s.ultrasound.z0 = 0;
s.ultrasound.interaction_distance = 3e-3;
%% light pulse settings
s.light_case = 'unitAreaGaussianPulse';
s.light.fwhm_power = 1e-6;%s


s.p.edges = s.voxel_edges;
s.wl = 800e-9;
%%  setting checks
% for checking when running with F9
if nargout == 0
	setupTaggedLightSimulator(s.ultrasound_case,s.light_case)
    close all
    
    %% plot ultrasound probe positions
    hold on
    plot3(s.ultrasound.x0*ones(size(s.probed_depths)),...
		s.ultrasound.y0*ones(size(s.probed_depths)),...
		s.probed_depths,...
		'rx') % only works for length 1 of y0
	title('simulation voxel edges and ultrasound probe positions')
	xlabel('x [m]')
	ylabel('y [m]')
	zlabel('z [m]')
    %% plot voxel edges
    
    for i = 1:numel(s.voxel_edges.z)
        [x,y,z] = meshgrid(s.voxel_edges.x,s.voxel_edges.y,s.voxel_edges.z(i));
        surf(x,y,z,'FaceColor','none','LineStyle','-')
        hold on
    end
	s.ultrasound.x0 = 0;
	s.ultrasound.y0 = 0;
	s.ultrasound.z0 = 0;
    %% plot ultrasound pulse in new figures;
	writeUltrasoundSettings(s.ultrasound)
    plotUSmm;
	writeLightSettings(s.light)
	[t,~,I]= plotLightPulse(s.S,s.Fs);
	pulse_energy = trapz(t,I,1)
else
    varargout{1}  =s;
end
end

