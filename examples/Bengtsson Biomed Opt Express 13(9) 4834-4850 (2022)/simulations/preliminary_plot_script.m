clearvars;
load ABCD.mat

N = size(I1,1); % number of probed positions
V = size(D,2); % number of voxels

depths = unique(map(2:end,3)); % probed depths (map contains all voxel centers
y_us = -2.5e-3;
y_inclusion = 2.5e-3;

T_sideband_on_inclusion = zeros(N,1);
T_sideband_off_inclusion= zeros(N,1);

T_carrier_on_inclusion  = zeros(N,1);
T_carrier_off_inclusion = zeros(N,1);

mu_bcg = 20;%m-1;
mu_inc = 40;%m-1;

for n = 1:N
    Index_ultrasound_voxel = all(map == [0 y_us depths(n)],2);
    Index_other_voxel = all(map == [0 y_inclusion depths(n)],2);


    mu = ones(V,1)*mu_bcg;
    mu(Index_ultrasound_voxel) = mu_inc;
    T_sideband_on_inclusion(n) = (I1(n,:)*exp(-D*mu))/Z;
    T_carrier_on_inclusion(n) = ((Ic(n,:)+1)*exp(-D*mu))/Z;

    mu = ones(V,1)*mu_bcg;
    mu(Index_other_voxel) = mu_inc;
    T_sideband_off_inclusion(n) = (I1(n,:)*exp(-D*mu))/Z;
    T_carrier_off_inclusion(n) = ((Ic(n,:)+1)*exp(-D*mu))/Z;

end
figure
hold on
semilogy(depths,T_sideband_off_inclusion,depths,T_sideband_on_inclusion)
xlabel('inclusion positiuon')
ylabel('Signal transmission')
set(gca,'YScale','log')
figure
semilogy(depths,T_carrier_off_inclusion,depths,T_carrier_on_inclusion)
xlabel('inclusion positiuon')
ylabel('Signal transmission')
set(gca,'YScale','log')

save('Photon_Transmissions','T_carrier_off_inclusion','T_carrier_on_inclusion','T_sideband_on_inclusion','T_sideband_off_inclusion','depths')