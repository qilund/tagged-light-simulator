clearvars

d1 = load("A.mat");
d2 = load("B.mat");

D = [d1.D;d2.D];
I1 = [d1.I1,d2.I1];
Ic = [d1.Ic,d2.Ic];
map = d1.map;
Z = d1.Z + d2.Z;
clearvars d1 d2
save('AB')

clearvars

d1 = load("C.mat");
d2 = load("D.mat");

D = [d1.D;d2.D];
I1 = [d1.I1,d2.I1];
Ic = [d1.Ic,d2.Ic];
map = d1.map;
Z = d1.Z + d2.Z;
clearvars d1 d2
save('CD')

clearvars

d1 = load("AB.mat");
d2 = load("CD.mat");

D = [d1.D;d2.D];
I1 = [d1.I1,d2.I1];
Ic = [d1.Ic,d2.Ic];
map = d1.map;
Z = d1.Z + d2.Z;
clearvars d1 d2
save('ABCD','-v7.3')
delete('A.mat','B.mat','C.mat','D.mat','AB.mat','CD.mat')
