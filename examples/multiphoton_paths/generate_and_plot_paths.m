
clearvars
close all

pick = 2;
%% multiphoton settings
%medium size and coxel boundaries
switch pick
    case 1
        edges.x = [-5 -2 5]*1e-2;
        edges.y = [-5 2  5]*1e-2;
        edges.z = [0 2 10]*1e-2;
    case 2
        edges.x = [-5 2.5 5]*1e-2;
        edges.y = [-5  5]*1e-2;
        edges.z = [0 2 3 5]*1e-2;
end
mc.edges = edges;
mc.homogeneous = false;
x = (edges.x(1:end-1)+edges.x(2:end))/2;
y = (edges.y(1:end-1)+edges.y(2:end))/2;
z = (edges.z(1:end-1)+edges.z(2:end))/2;
[X,Y,Z] = ndgrid(x,y,z);

%source spatial distribution
mc.source.type = 'point';
mc.source.center = [-0.0350 0.0350 0];
mc.source.start_c = mc.source.center;
% mc.source.radius = 35e-3/2;
mc.source.start_k = [0 0 1];

%detector area distribution
mc.detector.type = 'circle';
mc.detector.center = [2.5 -2.5 5]*1e-2; % will change in simulation loop
mc.detector.radius = 10e-3;

% voxel properties
mu_sp = ones(numel(edges.x)-1,numel(edges.y)-1,numel(edges.z)-1)*5*100;
switch pick
    case 1
        coordinate = [X(1,2,2),Y(1,2,2),Z(1,2,2)];
        mu_sp (1,2,2) = 100*100;
    case 2
        
        coordinate = [X(1,1,2),Y(1,1,2),Z(1,1,2)];
        mu_sp (1,1,2) = 100*100;
        mu_sp (1,1,3) = 1*100;
%         mu_sp (2,1,3) = 10*100;
end

g = ones(numel(edges.x)-1,numel(edges.y)-1,numel(edges.z)-1)*0.9;
mc.mu_s = mu_sp./g;
mc.g = g;

% refractive indices
mc.n_internal = 1;
mc.n_external = 1;

% number of paths per MC simulation.
mc.num_paths = 50e0;
%show progress
% mc.progressbar = true;

pp = [0 0 0;0 0 5;0 1 10]*1e-2;
[pp,Z,~] = multiphoton(mc);
plotpath(pp,mc.edges)
view(32,22)