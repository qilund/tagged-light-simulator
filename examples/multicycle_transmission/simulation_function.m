function simulation_function(file_name)

if ~((ischar(file_name)) || (isstring(file_name)))
   error('Input ''file_name'' must be a string or a char') 
end

mu_inc = 2		*100;%1/m
mu_bcg = 0.118	*100;%1/m

%% ultrasound field settings
us.type = 'multiCycleYProp';

us.x0 = 0;
us.y0 = 0;
us.z0 = 4.7e-2;
us.N_cycles = 100;
us.FWHM_z = 0.875e-3;
us.FWHM_x = 0.875e-3;
us.peak_pressure = 2.6e6/2;
us.frequency = 3.5e6;
us.max_distance = 3e-3;

%% light pulse settings
light.type = 'unitEnergySquare';
light.length = 1e-6;
light.fwhm_power = 1e-6;

%% tagged light simulation settings
S = 2^8; %no unit
Q = 15;% no unit
Fs = 42e6;%Hz %Nyquist criteria met for first 6 sidebands, interest is the first sideband only
wl = 606e-9; %m

%% monte carlo settings
%medium properties
mc.g = 0.6; %
mc.mu_s = 9/(1-mc.g)*100;%m-1
mc.n_internal = 1.33;
mc.n_external = 1;

%medium size
% mc.edges.x = [-4 (-0.075-1.1/2) (0.075-1.1/2) (-0.075+1.1/2) (0.075+1.1/2) 4]*1e-2;
mc.edges.x = [-4 (-1.1-0.075) (-1.1+0.075) -0.075 0.075 4]*1e-2;
mc.edges.y = [-4 -1.5 1.5 4]*1e-2;
mc.edges.z = [0 (4.7-0.075) (4.7+0.075) 9.4]*1e-2;

%source spatial distribution
mc.source.type = 'xy tophat';
mc.source.center = [0 0 0]*1e-2;
mc.source.radius = 35e-3/2;
mc.source.start_k = [0 0 1];

%detector area distribution
mc.detector.type = 'circle';
mc.detector.center = [0 0 9.4]*1e-2; % will change in simulation loop
mc.detector.radius = 12.7e-3/2;

% number of paths per MC simulation.
mc.num_paths = 10e0;
%show progress?
mc.progressbar = true;

%% initialize simulator
setupTaggedLightSimulator(us.type,light.type)
writeUltrasoundSettings(us)
writeLightSettings(light)


%% simulate paths
[tmpPaths,Z,~] = multiphoton(mc);
tmpPaths2 = [tmpPaths;cellfun(@mirrorPathsOverY0,tmpPaths,'UniformOutput',false)];
paths	  = [tmpPaths2;cellfun(@mirrorPathsOverX0,tmpPaths2,'UniformOutput',false)];
Z = Z*4;

clear tmp*
%% calculate D_matrix
[D_big,grid_map] = createDMatrix(paths,mc.edges,'full');
%grid_map(24,:) Ultrasound on voxel
%grid_map(22,:) Ultrasound off
%% collapse D matrix to 2 voxels
I_on_voxel = false(45,1);I_on_voxel(24) = true;
I_off_voxel = false(45,1);I_off_voxel(22) = true;
D(:,1) = sum(D_big(:,~(I_on_voxel|I_off_voxel)),2);
D(:,2) =  D_big(:,I_on_voxel);
D(:,3) = D_big(:,I_off_voxel);
%% calculate spectra with ultrasound
[spectra,f] = tagged_spectra_simulator_smart(paths,S,Q,Fs,wl,true);
Ac = signalSelection(f,spectra,0,us.frequency);
A1 = signalSelection(f,spectra,us.frequency,us.frequency);


%% calculate transmissions on inclusion
mu = [mu_bcg; mu_inc; mu_bcg];

T_carrier_us_on_inclusion = Ac/Z*exp(-D*mu);
T_sideband_us_on_inclusion = A1/Z*exp(-D*mu);

%% calculate transmission off inclusion with ultrasound
mu = [mu_bcg; mu_bcg; mu_inc];

T_carrier_us_off_inclusion  = Ac/Z*exp(-D*mu);
T_sideband_us_off_inclusion = A1/Z*exp(-D*mu);

%% calculate spectra with ultrasound
[spectra,f] = noInteractSpectra(S,Fs);
Ac_no_us = signalSelection(f,spectra,0,us.frequency); %scalar
% A1 = signalSelection(f,spectra,us.frequency,us.frequency);


%% calculate transmissions
mu1 = [mu_bcg; mu_inc; mu_bcg];
mu2 = [mu_bcg;mu_bcg; mu_inc];
T_carrier_no_us_on_inc = (sum(Ac_no_us/Z*exp(-D*mu1)));
T_carrier_no_us_off_inc = (sum(Ac_no_us/Z*exp(-D*mu2)));


clear paths D_big spectra

save(file_name)

end
	function P = mirrorPathsOverY0(P)
		P(:,2)=-P(:,2);
	end

	function P = mirrorPathsOverX0(P)
		P(:,1)=-P(:,1);
	end