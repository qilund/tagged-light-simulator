clearvars
close all

%% Multiphoton paramaters
N_thicknesses = 10;
thicknesses = (linspace(0.9,9.1,N_thicknesses))*1e-2;
thicknesses = sort([thicknesses [6.7500 5.5000 4.4500 3.4500 2.4500]*1e-2]);

T_carrier = zeros(N_thicknesses,1);
T_1st_sideband = T_carrier;

multiphoton_pars.edges.x = [-7 7]/2*1e-2;
multiphoton_pars.edges.y = [-7 7]/2*1e-2;

multiphoton_pars.g = 0.7;
mu_sp = 6.1*100;%m-1
multiphoton_pars.mu_s = mu_sp/(1-multiphoton_pars.g);

multiphoton_pars.source.type = 'point';
multiphoton_pars.source.start_k = [0,0,1];
multiphoton_pars.source.start_c = [0,0,0];

multiphoton_pars.detector.type = 'circle';

multiphoton_pars.detector.radius = 0.5e-2;

multiphoton_pars.num_paths = 20e3;
multiphoton_pars.n_internal = 1.33;
multiphoton_pars.n_external = 1;

multiphoton_pars.progressbar = false;

%% ultrasound parameters;

us.type = 'UOT2019';

us.x0 = 0;
us.y0 = 0;

us.sigma_x = [1.2, 4.6, 1.3]*1e-3;
us.sigma_y = 0.9e-3;
us.sigma_z = [0.8  1.1, 3.8]*1e-3;
us.peak_pressure = 3.5e6;
us.frequency = 1.6e6;
us.interaction_distance = 0.8e-2;%1 cm
us.R = [0.55 .17 0.28];
us.Nx = 8;
us.Nz = 4;
us.phi = repmat(pi/2,1,5);
us.pulse_name = 'X5-1 2mm';


%% light pulse parameters
light.type = 'unitAreaGaussianPulse';
light.fwhm_power = 1e-6;

%% TLS paramaters
S = 2^8; %no unit
Q = 10;% no unit
Fs = 20e6;%Hz
wl = 606e-9; %m


%% calculations
setupTaggedLightSimulator(us.type,light.type);

% 	plotUSmm('y-prop');
for i = 1:numel(thicknesses)

	thickness = thicknesses(i);%m
	multiphoton_pars.edges.z = [0 thickness];
	multiphoton_pars.detector.center = [0 0 thickness];
	us.z0 = thickness/2;


	writeLightSettings(light);
	writeUltrasoundSettings(us);
	disp(num2str(i))
	[paths,Z,~] = multiphoton(multiphoton_pars);
	disp('paths done')
	[spectra,f] = tagged_spectra_simulator_smart(paths,S,Q,Fs,wl);
	disp('tagging done');
	[carrier_spectra,carrier_f] = noInteractSpectra(S,Fs);
% 	save('raw_sim_output','spectra','f','paths','Z','multiphoton_pars','us',"light")
% 	load('raw_sim_output')
	f_width = 1e6;%Hz;
	f_center = 0;
	Ac = signalSelection(carrier_f,carrier_spectra,f_center,f_width);
	f_center = 1.6797e6;
	A1 = signalSelection(f,spectra,f_center,f_width);
% 	f_center = 1.67972e6*2;
% 	A2 = signalSelection(f,spectra,f_center,f_width);

	% for i = 1:numel(paths)
	% 	D(i) = sum(sqrt(sum((paths{i}(2:end) - paths{i}(1:end-1)).^2,1)));
	% end
	% D = D';
	D = createDMatrix(paths,[],'full');
	disp(' ')
	mu_a = 0.008*100;
	T_carrier(i)		= sum(1/Z * Ac*exp(-D*mu_a));
	T_1st_sideband(i)	= 1/Z * A1*exp(-D*mu_a);
% 	T_2nd_sideband(i)	= 1/Z * A2*exp(-D*mu_a);
	%

	% 	I0 = noInteractSpectra(S,Fs);
	% % 	Ac0 = repmat(signalSelection(f,I0,0,f_width),1,numel(paths));
	% 	T_carrier_0 = 1/Z * Ac0*exp(-D*mu_a);
	% 	T_1st_sideband/T_carrier_0;
	
% 	semilogy(thicknesses(1:i),T_carrier(1:i),thicknesses(1:i),T_1st_sideband(1:i))
% 	title(i)
% 	drawnow

end
save('data_transmission','T_*','thicknesses')