function E = Eo(t)
persistent E0 tau %#ok<PSET>
if isempty(E0)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
    load([myPath '\Light_settings.mat'])
end
E = E0.*exp(-(t).^2/(2*tau^2));
end

