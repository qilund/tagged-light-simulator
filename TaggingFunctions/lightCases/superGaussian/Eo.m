function E = Eo(t)
persistent E0 tau N %#ok<PSET>
if isempty(E0)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
    load([myPath '\light_settings.mat'])
end
E = E0.*exp(-(t).^N/2/tau.^N);
end

