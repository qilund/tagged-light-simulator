function writeLightSettings(settings)
%writeLightSettings writes the settings for the gaussian pulse light pulse
%INPUT:
%settings.peak_power - peak power in W for the  
%settings.fwhm_power - fwhm of pulse power
%OUTPUT:
% none

if ~isfield(settings,'N')||~isfield(settings,'peak_power') || ~isfield(settings,'FWHM')
   error('Gaussian pulse settings requires the fields ''FWHM'' and ''peak_power'' and ''N''') 
end
%% settings
N = settings.N;
tau = settings.FWHM/2/log(2)^(1/N);
epsilon = 8.854e-12;
c = 299792458;
E0 = sqrt(settings.peak_power/(c*epsilon));% electric field envelope amplitude.
%% file
P = mfilename('fullpath');
P = P(1:end-length(mfilename)-1);
files = dir(P);
for i = 3:numel(files)
    eval(['clear ' files(i).name]);
end
clear i settings
save([P '\Light_settings.mat'])
end

