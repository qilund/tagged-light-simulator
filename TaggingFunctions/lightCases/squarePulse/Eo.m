function E = Eo(t)
persistent E0 tau %#ok<PSET>
if isempty(E0)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
    load([myPath '\light_settings.mat'])
end
E = E0*(heaviside(t+1/2*tau)-heaviside(t-1/2*tau));
end

