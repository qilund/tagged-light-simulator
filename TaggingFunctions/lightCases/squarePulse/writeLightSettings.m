function writeLightSettings(settings)
%writeLightSettings writes the settings for the gaussian pulse light pulse
%INPUT:
%settings.peak_power - peak power in W for the  
%settings.fwhm_power - fwhm of pulse power
%OUTPUT:
% none

if ~isfield(settings,'length')||~isfield(settings,'power_amplitude')
   error('Gaussian pulse settings requires the fields ''fwhm_power'' and ''peak_power''') 
end
%% settings
tau = sqrt(settings.length.^2/4/log(2)); %fwhm of amplitude
epsilon = 8.854e-12;
c = 299792458;
E0 = sqrt(settings.power_amplitude/(c*epsilon));% electric field envelope amplitude.
%% file
P = mfilename('fullpath');
P = P(1:end-length(mfilename)-1);
files = dir(P);
for i = 3:numel(files)
    eval(['clear ' files(i).name]);
end
clear i settings c epsilon
save([P '\Light_settings.mat'])
end

