function writeLightSettings(settings)
%writeLightSettings writes the settings for the gaussian pulse light pulse
%INPUT:
%settings.fwhm_power - fwhm of pulse power
%OUTPUT:
% none

if ~isfield(settings,'length')
   error('unit area square pulse settings requires the fields ''length''') 
end
%% settings
tau = settings.length; %fwhm of amplitude
epsilon = 8.854e-12;
c = 299792458;
P_amp = 1/tau; % pamp*tau = 1
E0 = sqrt(P_amp/(c*epsilon));% electric field envelope amplitude.
%% file
P = mfilename('fullpath');
P = P(1:end-length(mfilename)-1);
files = dir(P);
for i = 3:numel(files)
    eval(['clear ' files(i).name]);
end
clear i settings c epsilon
save([P '\Light_settings.mat'])
end

