close all
clearvars
settings.taper_length = 0.6e-6;
settings.power_amplitude = 1;
settings.FWHM = 2e-6;
t = linspace(-5e-6,5e-6,1e3);
writeLightSettings(settings)
epsilon = 8.854e-12;
c = 299792458;
plot(t,Eo(t),t,Eo(t).^2*c*epsilon)
[~,i]= min((0.5-Eo(t).^2*c*epsilon).^2);
t(i)