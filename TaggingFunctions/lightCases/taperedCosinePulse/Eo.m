function E = Eo(t)
persistent E0 tau dt r%#ok<PSET>
if isempty(E0)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
    load([myPath '\light_settings.mat'])   
end
E = zeros(size(t));
Ic = (t <= tau) & (t >= -tau);
Il = (t < -tau) & t> (-tau - 2*dt);
Ir = (t > tau) & t< (tau + 2*dt);
E(Ic) = 1;
E(Ir) = 1/2*(1 + cos(2*pi/r*(t(Ir)-tau)));
E(Il) = 1/2*(1 + cos(2*pi/r*(t(Il)+tau)));
E = E*E0;
end

