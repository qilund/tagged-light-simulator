function writeLightSettings(settings)
%writeLightSettings writes the settings for the tapered cosine light pulse
%INPUT:
%settings.peak_power - peak power in W for the  
%settings.fwhm_power - fwhm of pulse power
%settings.taper       
%OUTPUT:
% none

if ~isfield(settings,'taper_length')...
   ||~isfield(settings,'power_amplitude')...
   ||~isfield(settings,'FWHM')
   error('Gaussian pulse settings requires the fields ''FWHM'', ''power_amplitude'', ''flat_length''') 
end
%% settings
% if settings.taper_length<settings.FWHM/2
%    error('flat_length must be smaller than FWHM') 
% end
dt = settings.taper_length;
r = 4*dt;
tau = settings.FWHM/2-r/2/pi*acos(sqrt(2)-1);
epsilon = 8.854e-12;
c = 299792458;
E0 = sqrt(settings.power_amplitude/(c*epsilon));% electric field envelope amplitude.
%% file
P = mfilename('fullpath');
P = P(1:end-length(mfilename)-1);
files = dir(P);
for i = 3:numel(files)
    eval(['clear ' files(i).name]);
end
clear i settings c epsilon
save([P '\Light_settings.mat'])
end

