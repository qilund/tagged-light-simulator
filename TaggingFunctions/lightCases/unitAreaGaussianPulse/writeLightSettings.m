function writeLightSettings(settings)
%writeLightSettings writes the settings for a gaussian pulse light pulse
%which has unit area under the pulse. Used for calculating power
%transmission
%
%INPUT: 
%settings.fwhm_power - fwhm of pulse power [s]
%
%OUTPUT:
% none

if ~isfield(settings,'fwhm_power')
   error('Gaussian pulse settings requires the fields ''fwhm_power''') 
end
%% settings
% pulse_energy = Peak_power*sqrt(pi/log(16))*abs(fwhm_power)
peak_power = 1/(sqrt(pi/log(16))*abs(settings.fwhm_power)); % unit 1/s, 
tau = sqrt(settings.fwhm_power.^2/4/log(2)); %tau = fwhm of amplitude
epsilon = 8.854e-12;
c = 299792458;
E0 = sqrt(peak_power/(c*epsilon));% electric field envelope amplitude.
%% file
P = mfilename('fullpath');
P = P(1:end-length(mfilename)-1);
files = dir(P);
for i = 3:numel(files)
    eval(['clear ' files(i).name]);
end
clear i settings
save([P '\Light_settings.mat'])
end

