function writeUltrasoundSettings(settings)
%writeUltraosundSettings -  writes the settings.mat for the custom1 pulse
%							Some settings are hardcoded for properties 
%							of water.
%INPUT:
%settings.peak_pressure 
%settings.x0            
%settings.y0            
%settings.z0
%settings.sigma_x
%settings.sigma_y1
%settings.sigma_y2
%settings.sigma_z
%settings.frequency
%settings.interaction_distance
%settings.R
%settings.a
%OUTPUT:
% none
%writes both variable and hardcoded settings for the  

%% input check
if	      ~isfield(settings,'x0') ...
        ||~isfield(settings,'y0') ...
        ||~isfield(settings,'z0') ...
        ||~isfield(settings,'interaction_distance')
   error('UOT2021_continous settings requires the fields''x0'', ''y0'', ''z0'', ''interaction_distance''') 
end

%% settings, hardcoded and from input settings
n0  = 1.3; % optical refractive index
C   = 1480; %m/s
sx  = [2.1 2.1 0.66 2.3 2.3]*1e-3;%m
sy1 = [1.9 1.9 0.50 0.9 0.9]*1e-3;%m
sy2 = [1.9 1.9 2.4 4 4]*1e-3;%m
sz  = 0.8*1e-3;
z_xtrap = [-100 15 20 30 100]*1e-3;
fa  = 6e6;%Hz
c	= [0.57 0.22 0.12 0.05 0.04];
R	= [1 1 0.5 0.6 0.6];
K   = 2*pi*fa/C;
Gamma0 = [3.8,3.8,9,3.4,3.4]*1e6;

x0  = settings.x0;%m
y0  = settings.y0;
z0  = settings.z0;
dndp = 1.466*1e-10;
c0   = 299792458;%speed of light
rho = 1000;%kg/m3
max_distance = settings.interaction_distance;
interaction_prediction_mode = 'point';
center_point = [x0,y0,z0];
%% saving settings file
P = mfilename('fullpath');
P = P(1:end-length(mfilename)-1);
files = dir(P);
for i = 3:numel(files)
    eval(['clear ' files(i).name]);
end
clear i settings files
save([P '\US_settings.mat'])
end
