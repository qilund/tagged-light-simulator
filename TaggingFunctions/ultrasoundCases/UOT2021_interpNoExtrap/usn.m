function n = usn(x,y,z,t)
persistent n0 C1 %#ok<PSET>
if isempty(n0)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
	load([myPath '\US_settings.mat'])
    C1 = n0*dndp;
end
n = n0 + C1.*usP(x,y,z,t);
end