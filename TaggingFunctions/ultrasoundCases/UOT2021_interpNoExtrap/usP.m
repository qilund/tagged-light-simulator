function P = usP(x,y,z,t)
persistent K x0 y0 z0 sy1 sy2 sx sz C Gamma0 c Rf R z_xtrap z_meas %#ok<PSET>
persistent sxf sy1f sy2f Gf
if isempty(K)
	myPath = mfilename('fullpath');
	myPath = myPath(1:end-length(mfilename)-1);
	load([myPath '\US_settings.mat'])

% 	Gf   = @(t) interp1(z_meas,Gamma0,C*t+z0,'linear','extrap');
	sxf  = @(z) interp1(z_xtrap,sx, z,'linear','extrap');
	sy1f = @(z) interp1(z_xtrap,sy1,z,'linear','extrap');
	sy2f = @(z) interp1(z_xtrap,sy2,z,'linear','extrap');

	Rf  = @(t) interp1(z_xtrap,R ,C*t+z0,'linear','extrap');
% % 	szf = @(t) interp1(z_xtrap,sz,C*t+z0,'linear','extrap');
	Gf  = @(t) interp1(z_xtrap,Gamma0,C*t+z0);
end
	P = Gf(t).*...
		exp(-(x - x0).^2./(sxf(z).^2)).*...
		(Rf(t).*exp(-(y- y0).^2./(sy1f(z).^2)) + ...
		(1-Rf(t)).*exp(-(y- y0).^6./(sy2f(z).^6))).* ...
		exp(-(C.*t - (z- z0)).^4./(sz.^4)) ...
		.*(c(1)*sin( -K*(C*t - (z - z0)) + pi/2) + ...
		 c(2)*sin(-2*K*(C*t - (z - z0)) + pi/2) + ...
		 c(3)*sin(-3*K*(C*t - (z - z0)) + pi/2) + ...
		 c(4)*sin(-4*K*(C*t - (z - z0)) + pi/2) + ...
		 c(5)*sin(-5*K*(C*t - (z - z0)) + pi/2));
end

