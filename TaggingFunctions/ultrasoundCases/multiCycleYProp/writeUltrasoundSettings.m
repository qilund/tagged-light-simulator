function writeUltrasoundSettings(settings)
%writeUltraosundSettings - writes the settings.mat for the simplegaussian 
%                            pulse ultraosound pulse. Some settings are
%                            hardcoded for properties of water.
%INPUT:
%settings - struct with settings for given ultrasound case.
%
%OUTPUT:
% none
%writes both variable and hardcoded settings for the  

%% input check
if ~isfield(settings,'FWHM_x')...
		||~isfield(settings,'N_cycles')...
		||~isfield(settings,'FWHM_z')...
        ||~isfield(settings,'x0') ...
        ||~isfield(settings,'y0') ...
        ||~isfield(settings,'z0') ...
        ||~isfield(settings,'peak_pressure')...
        ||~isfield(settings,'frequency')...
		||~isfield(settings,'max_distance')
   error('Cycle utrasound beam requires the settings ''x0'', ''y0'', ''z0'', ''FWHM_x'', ''FWHM_z'', ''peak_pressure'', ''frequency'', ''N_cycles'', ''max_distance''') 
end

%% settings, hardcoded and from input settings
n0 = 1.3; % optical refractive index
C = 1480; % m/s
sx = settings.FWHM_x/2/sqrt(log(2));% m
sz = settings.FWHM_z/2/sqrt(log(2));% m
fa = settings.frequency;% Hz
sy = (settings.N_cycles-0.5)/2*C/fa;% m
K = 2*pi*fa/C;%1/m
Gamma0 = settings.peak_pressure;% Pa
x0 = settings.x0;% m
y0 = settings.y0;% m
z0 = settings.z0;% m
dndp = 1.466*1e-10;% 1
c = 299792458; % m/s
rho = 1000;% kg/m3
max_distance =settings.max_distance;
line_point = [x0,y0,z0];
line_vector = [0 1 0];
interaction_prediction_mode = 'line';
%% saving settings file
P = mfilename('fullpath');
P = P(1:end-length(mfilename)-1);
files = dir(P);
for i = 3:numel(files)
    eval(['clear ' files(i).name]);
end
clear i settings
save([P '\US_settings.mat'])
end
