clearvars
close all
us.frequency = 3.5e6;
us.y0 = 0;
us.x0 =0;
us.z0 = 0;
us.FWHM_x = 1e-3;
us.FWHM_z = 1e-3;
us.N_cycles = 10;
us.peak_pressure = 2e6;
setupTaggedLightSimulator('multiCycleYProp','unitAreaGaussianPulse')
writeUltrasoundSettings(us)


plotUSmm('y-prop');