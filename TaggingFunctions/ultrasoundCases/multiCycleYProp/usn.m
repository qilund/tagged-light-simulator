function n = usn(x,y,z,t)
persistent n0 C1 %#ok<PSET>
if isempty(n0)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
    dat = load([myPath '\US_settings.mat']);
	n0 = dat.n0;
	dndp = dat.dndp;
    C1 = n0*dndp;
end
n = n0+C1.*usP(x,y,z,t);
end