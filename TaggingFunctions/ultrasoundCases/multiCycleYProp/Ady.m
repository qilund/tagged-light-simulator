function dy = Ady(x,y,z,t)
persistent C x0 y0 z0 sy sx sz K C1 %#ok<PSET>
if isempty(C)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
    load([myPath '\US_settings.mat'])
    C1 = Gamma0./(rho.*K.*C.^2); %collecting constants
end
O = @(s) heaviside(s);
dy = C1*exp( - ((x-x0)/sx).^2 - ((z-z0)/sz).^2).*...
	(-O(-(C.*t - (y- y0-sy))).*exp(-(C.*t - (y- y0-sy)).^2*K.^2) + ...
	O(C.*t - (y - y0 + sy)).*exp(-(C.*t - (y- y0+sy)).^2*K.^2) + ...
	(O(C.*t - (y- y0 + sy)) - O(C.*t - (y- y0-sy))).*...
	(sin(K*(C*t - (y - y0)) + 3*pi/2)));
end

