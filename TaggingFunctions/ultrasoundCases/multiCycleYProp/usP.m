function P = usP(x,y,z,t)
persistent K x0 y0 z0 sy sx sz C Gamma0 %#ok<PSET>
if isempty(K)
	myPath = mfilename('fullpath');
	myPath = myPath(1:end-length(mfilename)-1);
	load([myPath '\US_settings.mat'])
end
O = @(s) heaviside(s);
P = Gamma0*exp( - ((x-x0)/sx).^2 - ((z-z0)/sz).^2).*...
	(-O(-(C.*t - (y- y0-sy))).*exp(-(C.*t - (y- y0-sy)).^2*K.^2) + ...
	O(C.*t - (y - y0 + sy)).*exp(-(C.*t - (y- y0+sy)).^2*K.^2) + ...
	(O(C.*t - (y- y0 + sy)) - O(C.*t - (y- y0-sy))).*...
	(sin(K*(C*t - (y - y0)))));
end

