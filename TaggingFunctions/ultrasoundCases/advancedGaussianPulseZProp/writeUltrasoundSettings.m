function writeUltrasoundSettings(settings)
%writeUltraosundSettings - writes the settings.mat for the simplegaussian 
%                            pulse ultraosound pulse. Some settings are
%                            hardcoded for properties of water.
%INPUT:
%settings.peak_pressure 
%settings.x0            
%settings.y0            
%settings.z0
%settings.sigma_x
%settings.sigma_y
%settings.sigma_z
%settings.frequency
%OUTPUT:
% none
%writes both variable and hardcoded settings for the  

%% input check
if ~isfield(settings,'sigma_x')...
        ||~isfield(settings,'sigma_y')...
        ||~isfield(settings,'sigma_z')...
        ||~isfield(settings,'x0') ...
        ||~isfield(settings,'y0') ...
        ||~isfield(settings,'z0') ...
        ||~isfield(settings,'peak_pressure')...
        ||~isfield(settings,'frequency') ...
        ||~isfield(settings,'interaction_distance')
   error('advanced gaussian pulse settings requires the fields ''width'', ''x0'',''y0'',''z0'',''peak_pressure'', ''frequency'', ''interaction_distance''') 
end

%% settings, hardcoded and from input settings
n0 = 1.3; % optical refractive index
C = 1480; %m/s
sx = settings.sigma_x;%m
sy = settings.sigma_y;%m
sz = settings.sigma_z;%m
fa = settings.frequency;%Hz
K = 2*pi*fa/C;
Gamma0 = settings.peak_pressure;
x0 = settings.x0;%m
y0 = settings.y0;
z0 = settings.z0;
dndp = 1.466*1e-10;
c = 299792458;
rho = 1000;%kg/m3
max_distance = settings.interaction_distance;
interaction_prediction_mode = 'point';
center_point = [x0,y0,z0];
%% saving settings file
P = mfilename('fullpath');
P = P(1:end-length(mfilename)-1);
files = dir(P);
for i = 3:numel(files)
    eval(['clear ' files(i).name]);
end
clear i settings files
save([P '\US_settings.mat'])
end
