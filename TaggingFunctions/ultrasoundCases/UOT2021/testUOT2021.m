% test
clear all
an =[1 0.40 0.22 0.094 0.080];



s.sigma_x = 0.66e-3; % OK hard to motivate anything else
s.sigma_y1 = 0.42e-3; % will need "wings"
s.sigma_y2 = 1.9e-3;
s.sigma_z = 0.8e-3; %OK, maybe a bit smaller
s.R =  [.5 0.5];
s.a = an./sum(an);
s.x0 = 0;
s.y0 = 0;
s.z0 = 0;
s.peak_pressure = 9e6;
s.frequency = 6e6;
s.interaction_distance = 3e-3;
writeUltrasoundSettings(s);
Adz(0,0,0,0);
usP(0,0,0,0);
usn(0,0,0,0);
plotUSmm
% Va = 1480;
% Ka = 2*pi*fa/Va;
% zs = 0e-3;

