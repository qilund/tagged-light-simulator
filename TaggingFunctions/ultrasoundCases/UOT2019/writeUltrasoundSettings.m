function writeUltrasoundSettings(settings)
%writeUltraosundSettings -  writes the settings.mat for the custom1 pulse
%							Some settings are hardcoded for properties
%							of water.
%INPUT:
%settings.peak_pressure
%settings.x0
%settings.y0
%settings.z0
%settings.sigma_x
%settings.sigma_y1
%settings.sigma_y2
%settings.sigma_z
%settings.frequency
%settings.interaction_distance
%settings.R
%settings.a
%OUTPUT:
% none
%writes both variable and hardcoded settings for the

%% input check
if		  ~isfield(settings,'sigma_x')...
		||~isfield(settings,'sigma_y') ...
		||~isfield(settings,'sigma_z')...
		||~isfield(settings,'x0') ...
		||~isfield(settings,'y0') ...
		||~isfield(settings,'z0') ...
		||~isfield(settings,'peak_pressure')...
		||~isfield(settings,'frequency') ...
		||~isfield(settings,'interaction_distance')...
		||~isfield(settings,'R') ...
		||~isfield(settings,'Nx') ...
		||~isfield(settings,'Nz') ...
		||~isfield(settings,'phi') ...
		||~isfield(settings,'pulse_name')
	error('UOT2019 settings requires the fields ''sigma_x'', ''sigma_y'', ''sigma_z'', ''x0'',''y0'',''z0'',''peak_pressure'', ''frequency'', ''interaction_distance'', ''R'', ''Nx'', ''Nz'', ''pulse_name'', ''phi''')
end

%% settings, hardcoded and from input settings
% refractive index
n0  = 1.3; % optical refractive index %HARDCODED

% Speed of sound
C   = 1480; %m/s %HARDCODED

% Pulse widths, sx and sz are vectors with 3 elements;
sx  = settings.sigma_x;%m
sy  = settings.sigma_y;%m
sz  = settings.sigma_z;%m

% fundamental frequency of ultrasound;
fa  = settings.frequency;%Hz

% Relative height of ultrasound envelope parts, vector with 3 elements
R	= settings.R;

% Super gaussian integers for envelope, even integers
Nx	= settings.Nx;
Nz  = settings.Nz;

% Wave number
K   = 2*pi*fa/C;

% Peak pressure
Gamma0 = settings.peak_pressure;

% pulse center at t = 0;
x0  = settings.x0;%m
y0  = settings.y0;
z0  = settings.z0;

%piezooptical coefficient
dndp = 1.466*1e-10; % 1/Pa HARDCODED

% speed of light in vacuum
c   = 299792458;  %HARDCODED
%medium density
rho = 1000;%kg/m3, %HARDCODED water
% max distance before no light-ultrasound interaction with a path
max_distance = settings.interaction_distance;
interaction_prediction_mode = 'point';
center_point = [x0,y0,z0];
%phase of carrier wave
phi = settings.phi;

% relative heights of the harmonic frequencies in the envelope.
switch settings.pulse_name
	case 'X5-1 4mm'
		tmpP1 = [-0.004162382519886,0.061639430121152,-0.003236713966835];
		tmpP2 = [0.015561779584702,-0.011779106187661];
		tmpP3 = [0.006175705011526,-0.002487052504412];
		tmpP4 = [0.002949814536922,-0.002700437084724];
		tmpP5 = [0.001577556820425,-0.001179606797690];
		% The X5-1 4mm pulse length was not constant for different amplitudes. 
		tmpGlobalFactor =  0.00039361;
		tmpSlopeFactor  = 2.2486e-06;
		tmpP0           =  1100000;
		tmpOffset       =  0.0013347;
		
		tmpsyF = @(P) tmpGlobalFactor*1./(exp(-tmpSlopeFactor*(P-tmpP0))+1)+tmpOffset;
		sy = tmpsyF(Gamma0);
	case 'X5-1 2mm'
		tmpP1 = [-0.004126521309822,0.062781300918462,0.002133563091309];
		tmpP2 = [0.018088130918917,-0.015994069622402];
		tmpP3 = [0.007854093948037,-0.006300632442315];
		tmpP4 = [0.004305843633583,-0.004766418908807];
		tmpP5 = [0.002423054722741,-0.002682761611298];
	case 'X5-1 0.5mm'
		tmpP1 = [-8.001149399711275e-04,0.021496062735106,0.003781082840206];
		tmpP2 =[0.007080453406199,-0.003795932883448];
		tmpP3 =[0.002990387711235,-0.003718787529787];
		tmpP4 = [0.002341555702416,-0.003318581502455];
		tmpP5 = [0.001692961933537,-0.002745153761806];
	case 'L12-3 2mm' 
		tmpP1  =  [0.22645     0.11683];
		tmpP2  =  [0.13086    -0.07358];
		tmpP3  =  [0.078426    -0.11076];
		tmpP4  =  [0.064785    -0.11099];
		tmpP5  =  [0.048175   -0.090093];
	case 'L12.3 4mm'
		tmpP1  =  [0.23     0.12];
		tmpP2  =  [0.13    -0.074];
		tmpP3  =  [0.078    -0.11];
		tmpP4  =  [0.064    -0.11];
		tmpP5  =  [0.048   -0.09];
end

tmpIp  = [heaviside(polyval(tmpP1,Gamma0*1e-6)).*polyval(tmpP1,Gamma0*1e-6)+1e-9; % Pressure frequency intentsities
	(heaviside(polyval(tmpP2,Gamma0*1e-6)).*polyval(tmpP2,Gamma0*1e-6));
	(heaviside(polyval(tmpP3,Gamma0*1e-6)).*polyval(tmpP3,Gamma0*1e-6));
	(heaviside(polyval(tmpP4,Gamma0*1e-6)).*polyval(tmpP4,Gamma0*1e-6))
	(heaviside(polyval(tmpP5,Gamma0*1e-6)).*polyval(tmpP5,Gamma0*1e-6))];

a = tmpIp/sum(tmpIp);

%% saving settings file
P = mfilename('fullpath');
P = P(1:end-length(mfilename)-1);
files = dir(P);
for i = 3:numel(files)
	eval(['clear ' files(i).name]);
end
clear i settings files tmp* tmpsyF
save([P '\US_settings.mat'])
end
