function n = usn(x,y,z,t)
persistent n0 K x0 y0 z0 sx sy sz C a R C1 phi sink Nx Nz %#ok<PSET>
if isempty(n0)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
	dat = load([myPath '\US_settings.mat'],'n0', 'K', 'x0', 'y0', 'z0', 'sx', 'sy', 'sz','C', 'a', 'R', 'phi', 'Nx', 'Nz','dndp','Gamma0');
	n0 = dat.n0;
	K = dat.K;
	x0 = dat.x0; y0 = dat.y0; z0 = dat.z0;
	sx = dat.sx; sy = dat.sy; sz = dat.sz;
	C = dat.C;
	a = dat.a;
	R = dat.R;
	phi = dat.phi;
	Nx = dat.Nx; Nz = dat.Nz;
    C1 = n0*dat.dndp*dat.Gamma0;
	sink = @(x) sinc(x/pi);
end
n = n0 + C1.*...
	exp(-(C.*t - (y-y0)).^2./(2.*sy.^2)).*...
	(R(1).*sink((x - x0)./(sx(1))).^2.*sink((z - z0)./(sz(1))).^2 + ...
	R(2).*exp(-((x-x0).^Nx)/(Nx*sx(2).^Nx)).*exp(-((z-z0).^2)/(2*sz(2).^2)) + ...
	R(3).*exp(-((x-x0).^2)/(2*sx(3).^2)).*exp(-((z-z0).^Nz)/(Nz*sz(3).^Nz))).*...
	(a(1)*sin(  K*(C*t - (y - y0)) + phi(1)) + ...
	a(2) *sin(2*K*(C*t - (y - y0)) + phi(2)) + ...
	a(3) *sin(3*K*(C*t - (y - y0)) + phi(3)) + ...
	a(4) *sin(4*K*(C*t - (y - y0)) + phi(4)) + ...
	a(5) *sin(5*K*(C*t - (y - y0)) + phi(5)));
end