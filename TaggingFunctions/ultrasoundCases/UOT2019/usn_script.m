% USN_SCRIPT   Generate MEX-function usn_mex from usn.
% 
% Script generated from project 'usn.prj' on 14-Oct-2022.
% 
% See also CODER, CODER.CONFIG, CODER.TYPEOF, CODEGEN.

%% Create configuration object of class 'coder.MexCodeConfig'.
cfg = coder.config('mex');
cfg.GenerateReport = true;
cfg.ReportPotentialDifferences = false;

%% Define argument types for entry-point 'usn'.
ARGS = cell(1,1);
ARGS{1} = cell(4,1);
ARGS{1}{1} = coder.typeof(0,[1024 100000   21],[1 1 1]);
ARGS{1}{2} = coder.typeof(0,[1024 100000   21],[1 1 1]);
ARGS{1}{3} = coder.typeof(0,[1024 100000   21],[1 1 1]);
ARGS{1}{4} = coder.typeof(0,[1024 100000   21],[1 1 1]);

%% Invoke MATLAB Coder.
cd('D:\Users\da1024hi\MATLAB\tagged-light-simulator\TaggingFunctions\ultrasoundCases\UOT2019');
codegen -config cfg usn -args ARGS{1}

