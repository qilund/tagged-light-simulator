us_type = 'UOT2021_continous';

us.x0 = 0;
us.y0 = 0;
us.z0 = 15e-3;
us.interaction_distance = 10e-3;

light_pulse_type = 'unitAreaGaussianPulse';
light.fwhm_power = 1e-6;

setupTaggedLightSimulator(us_type,light_pulse_type);
writeLightSettings(light);
writeUltrasoundSettings(us);
plotUSmm('z-prop',1)
cleanupTaggedLightSimulator;

z0 = [0 7.5 15 20 30]*1e-3;
for i = 1:numel(z0)
	us.z0 = z0(i);
	max_distance = 10e-3;
	us.interaction_distance = max_distance;
	writeUltrasoundSettings(us);

	y = linspace(-max_distance,max_distance,1e2+1)+us.y0;
	x = linspace(-max_distance,max_distance,1e2+1)+us.x0;
	z = linspace(-max_distance,max_distance,1e2+1)+us.z0;
	t = 0;
	[x,y,z] = ndgrid(x,y,z);
	E(i) = sum(usP(x,y,z,0).^2,"all");
end
E