
function [r_US,intDist] = interactDist

persistent x0 y0 z0 interaction_distance%#ok<PSET>
if isempty(x0)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
    load([myPath '\US_settings.mat'])
    intDist = interaction_distance;
    r_US = [x0,y0,z0];
end
intDist = interaction_distance;
r_US = [x0,y0,z0];
end