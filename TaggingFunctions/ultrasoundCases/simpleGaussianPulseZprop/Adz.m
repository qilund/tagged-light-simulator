function dz = Adz(x,y,z,t)
persistent C x0 y0 z0 sy sx sz K C1 %#ok<PSET>
if isempty(C)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
    load([myPath '\US_settings.mat'])
    C1 = Gamma0./(rho.*K.*C.^2); %collecting constants
end
dz = C1.*exp(-(x- x0).^2./(2.*sx.^2)...
    -(y - y0).^2./(2.*sy.^2)).*...
    exp(-(C.*t - (z- z0)).^2./(2.*sz.^2)).* ...
    (sin(K*(C*t - (z - z0))));
end