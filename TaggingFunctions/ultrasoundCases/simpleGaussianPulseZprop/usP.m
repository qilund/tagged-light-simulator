function P = usP(x,y,z,t)
persistent K x0 y0 z0 sy sx sz C Gamma0 %#ok<PSET>
if isempty(K)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
    load([myPath '\US_settings.mat'])  
end
P = Gamma0.*exp(-(x- x0).^2./(2.*sx.^2)...
    -(y - y0).^2./(2.*sy.^2)).*...
    exp(-(C.*t - (z- z0)).^2./(2.*sz.^2)).* ...
    (sin(K*(C*t - (z - z0)) + pi/2));
end

