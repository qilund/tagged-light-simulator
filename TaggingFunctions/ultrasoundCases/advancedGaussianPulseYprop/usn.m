function n = usn(x,y,z,t)
persistent n0 K x0 y0 z0 sy sx sz C1 C %#ok<PSET>
if isempty(n0)
    myPath = mfilename('fullpath');
    myPath = myPath(1:end-length(mfilename)-1);
    dat = load([myPath '\US_settings.mat']);
	n0 = dat.n0;
	K = dat.K;
	x0 = dat.x0; y0 = dat.y0; z0 = dat.z0;
	sx = dat.sx; sy = dat.sy; sz = dat.sz;
	C = dat.C; 
	dndp = dat.dndp;
	Gamma0 =  dat.Gamma0;
    C1 = n0*dndp*Gamma0;
end
n = n0+C1.*exp(-(x- x0).^2./(2.*sx.^2)...
    -(z - z0).^2./(2.*sz.^2)).*...
    exp(-(C.*t - (y- y0)).^4./(4.*sy.^4)).* ...
    (sin(K*(C*t - (y - y0) + pi/2)));
end