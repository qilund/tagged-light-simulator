function varargout = plotUSmm(propmode,plot_sequence)
% plotUSmm
% Default Assumes a Z-propagation of the ultrasound. Plots the ultrasound pulse
% around the defined [x0,y0,z0] and at t = 0
%
% Inputs:
%	mode - string with either "z-prop", "y-prop" or "x-prop"
%
% Outputs:
% h1 - figure handle to first figure (y-z plot)
% h2 - figure handle to second figure (x-z plot)



if nargin == 0
	propmode = 'z-prop';
	plot_sequence = 0;
elseif nargin  == 1
plot_sequence = 0;
end


switch propmode

	case 'z-prop'
		O = plotZprop(plot_sequence);
	case 'y-prop'
		O = plotYprop(plot_sequence);
end
varargout = O;
end

function O = plotZprop(plot_sequence)
load('US_settings','x0','y0','z0','max_distance')
figure
y = linspace(-max_distance,max_distance,1e2+1)+y0;
x = x0;
z = linspace(-max_distance,max_distance,1e3+1)+z0;
[y,z] = ndgrid(y,z);
if plot_sequence
	t = linspace(-5 ,5,101)*1e-6;
else
	t = 0;
end
for i = 1:numel(t)
	P = usP(x,y,z,t(i));
	O{1} = surf(y*1e3,z*1e3,P/1e6,'LineStyle','none', 'FaceAlpha',0.5);
	view(0,0)
	
	xlabel('y [mm]')
	ylabel('z [mm]')
	zlabel('P [MPa]')
	axis([1e3*min(y(:)) 1e3*max(y(:)) 1e3*min(z(:)) 1e3*max(z(:)) -5 10])
	drawnow
end


figure
y = y0;
x = linspace(-max_distance,max_distance,1e2+1) + x0;
z = linspace(-max_distance,max_distance,1e3+1) + z0;
[x,z] = ndgrid(x,z);

if plot_sequence
	t = linspace(-5 ,5,101)*1e-6;
else
	t = 0;
end
for i = 1:numel(t)
	P = usP(x,y,z,t(i));
	O{2} = surf(x*1e3,z*1e3,P/1e6,'LineStyle','none', 'FaceAlpha',0.5);
	view(2)
	drawnow
	xlabel('x [mm]')
	ylabel('y [mm]')
	zlabel('P [MPa]')
end
end

function O = plotYprop(plot_sequence)
load('US_settings','x0','y0','z0','max_distance')
figure
y = linspace(-max_distance,max_distance,1e3+1)+y0;
x = x0;
z = linspace(-max_distance,max_distance,1e2+1)+z0;
[y,z] = ndgrid(y,z);
t = 0;
P = usP(x,y,z,t);
O{1} = surf(y*1e3,z*1e3,P/1e6,'LineStyle','none', 'FaceAlpha',0.5);
% h.CDataSource = 'P';
% set(h,'CData',P)
refreshdata
xlabel('y [mm]')
ylabel('z [mm]')
zlabel('P [MPa]')
hold on


figure
y = linspace(-max_distance,max_distance,1e3+1) + y0;
x = linspace(-max_distance,max_distance,1e2+1) + x0;
z = z0;
[x,y] = ndgrid(x,y);
if plot_sequence
	t = linspace(-5, 5,101)*1e-6;
else
	t = 0;
end
for i = 1:numel(t)
	P = usP(x,y,z,t(i));
	O{2} = surf(x*1e3,y*1e3,P/1e6,'LineStyle','none', 'FaceAlpha',0.5);
	view(2)
	drawnow
	xlabel('x [mm]')
	ylabel('y [mm]')
	zlabel('P [MPa]')
	
end
end