function [D,grid_map,Vmat] = createDMatrix(paths,edges,type)
%createDMatrix calculates the distance travelled of each path in each
%voxel defined by the grid with defined by the edges struct.
%
% INPUT:
% paths - cell with M paths or matrix with single path (M=1)
% edges.x - array with the boundaries of the x grid
% edges.y - array with the boundaries of the y grid
% edges.z - array with the boundaries of the z grid
% type	  - either 'full' or 'sparse'. Sets output type of D matrix
%
% OUTPUT
% D        - [M x V] matrix. D(m,v) = distance of path m in voxel v.
% grid_map - [V x 3] array. grid_map(v,:) = coordinates of voxel v.
% Vmat	   - [1 x 3] matrix with number of voxels in x y and z directions
%
% Author: David Hill 2022

if isempty(paths)
	D = [];
	[grid_map,Vmat] =create_grid_map(edges);
	return
end

if isempty(edges)
	homogeneous = true;
	grid_map= []; Vmat = [];
else
	homogeneous = numel(edges.x) + numel(edges.y) + numel(edges.z) == 6;
	[grid_map,Vmat,I] =create_grid_map(edges);
end
fullDist = @(p) sum(sqrt(sum((p(1:end-1,:) - p(2:end,:)).^2,2)));


if iscell(paths)
	if homogeneous
		D = cellfun(fullDist,paths);
	else

		M = numel(paths);
		D = cell(M,1);
		parfor m = 1:M
			tmpD = singlePathDmatrix(paths{m},edges,m);
			switch type
				case 'full'
					D{m} = tmpD(I)';
				case 'sparse'
					D{m} = sparse(tmpD(I)');
				otherwise
					error('type is ''full'' or ''sparse''')
			end
		end
		D = cell2mat(D);
	end
else
	if homogeneous
		D = fullDist(paths);
	else
		tmpD = singlePathDmatrix(paths,edges);

		D=tmpD(I)';
	end
end

end

function [grid_map,Vmat,I] =create_grid_map(edges)
%  creates the grid map and V matrix
xcoords = (edges.x(2:end)+edges.x(1:end-1))/2;
ycoords = (edges.y(2:end)+edges.y(1:end-1))/2;
zcoords = (edges.z(2:end)+edges.z(1:end-1))/2;
Vmat = [numel(xcoords),numel(ycoords),numel(zcoords)];

[X,Y,Z] = ndgrid(xcoords,ycoords,zcoords);
grid_map = [X(:),Y(:),Z(:)];
[grid_map,I] = sortrows(grid_map);

end


