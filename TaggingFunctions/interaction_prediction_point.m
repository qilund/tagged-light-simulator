function interactNodes = interaction_prediction_point(photPath,r0_US,interactionDistance)
%INTERACTION_PREDICTION_POINT gives boolean if a given path is expected to
%interact with the ultrasound propagating around a point. No requirement on
%units (SI units recommended)
%
% Inputs:
% photPath			  - photon path, a [J x 3] matrix with scattering
%						coordinates, or nodes.
% r0_US				  - center coordinate of ultrasound interaction
% interactionDistance - distance from r0_US which constitutes the maximum
%						distance for interaction with the ultrasound.
%
% Outputs:
% interactNodes		  - the nodes/coordinates in photPath which interact
%						with the ultrasound.
%
% See also INTERACTION_DISTANCE_LINE

%% Pseudocode
%
% calculate kVectors
% start point of each free path: a . end point: b. US_r0 = p
% <u,v> denotes scalar product
% v = (p-a)  - <(p-a),k>*k  is the min distance vector to the line defined
% by a and b
% minimum dist point on line: c = p + v
%
% 3 cases:  i)  c is inside free path length: dist = abs(v)
%          ii)  c is before a on line:  dist = abs(a-p)
%         iii)  c is after b on line:   dist = abs(b-p)
%
% for case i): add both nodes to interactNodes.
% for case ii and iii): add the node that is close and the next and
% previous nodes in fotPath to interactNodes.
%
%
% Author: David Hill


% if interactdistance is infinite, return all ones
if isinf(interactionDistance)
	interactNodes =true(size(photPath,1),1);
	return
end


%% real code
%        b: j=1:J          a: j= 0:J-1
k = photPath(2:end,:) - photPath(1:end-1,:);
% k = (k./sqrt(sum(k.^2,2)));
% p = repmat(US_r0,size(k,1),1);
ID_sqr = interactionDistance.^2;
% v = (a-p)                - <(a-p),k>*k
v = (photPath(1:end-1,:) - r0_US)  - sum((photPath(1:end-1,:)-r0_US).*k,2).*k./sum(k.^2,2);

c = r0_US + v;
kk = c-photPath(1:end-1,:);
I2 = any(0>kk.*k,2);
kk = c-photPath(2:end,:);
I3 = any(0<kk.*k,2);
I1 = (~I2)&(~I3)&(sum(v.^2,2)<ID_sqr);
% I2 = I2&sum((fotPath(1:end-1,:)-US_r0.^2),2)<
I1 = [I1;false] | [false;I1];
I2 = sum( (photPath-r0_US).^2,2)<ID_sqr;
I2 = I2 | [I2(2:end);false] | [false;I2(1:end-1)];

interactNodes = I1|I2;

end

