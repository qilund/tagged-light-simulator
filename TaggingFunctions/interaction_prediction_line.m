function [interactNodes] = interaction_prediction_line(photPath,line_point,line_vector, max_distance)
%INTERACTION_PREDICTION_LINE gives boolean if a given path is expected to
%interact with the ultrasound propagating along a line. No requirement on 
% units (SI units recommended)

% Inputs:
% photPath			  - photon path, a [J x 3] matrix with scattering
%						coordinates, or nodes.
% line_point		  - a point on the line to be measured to.
% interactionDistance - a vector paralell to the line measured to.
%
% Outputs:
% interactNodes		  - the nodes/coordinates in photPath which interact
%						with the ultrasound.
%% Definitions
% start point of each free path: a . end point: b. US_r0 = p
% <u,v> denotes scalar product, u x v the cross product.
% each free path defines a line l1 = p1 + t1*v1
% the line to be measured to is l2 = p2 - t2*v2
% the line wich is normal to both l1 and l2 is l3 = p1 + t1*v1 + t3*v3
% where v3 = v1 x v2.
%
% %math
% for l2 = l3 we define l3 as the line which connects l1 and l2 with the
% shortest minimum path.
% 
% p1 + t1*v1 + t3*v3 = p2 - t2*v2
% <=>
% t1*v1 + t2*v2 + t3*v3 = p2-p1 , ( t2' = -t2 )
% <=>
% V * T = p2 - p1, V = (V1, V2, V3), T = (t1;t2;t3)
%
% T = V\(p2-p1)
% 
% closest point c = p1 + t1*v1
% Pseudocode
% for free path points fp_i and fp_i+1, v1i = fp_i+1 - fp_i
% 
% 3 cases:  i)  c is inside free path length: dist = abs(v)
%          ii)  c is before a on line: dist = abs(a-p)
%         iii)  c is after  b on line: dist = abs(b-p)
%
% for case i): add both nodes to interactNodes.
% for case ii and iii): add the node that is close and the next and
% previous nodes in fotPath to interactNodes.
%
% Author: David Hill

line_vector = line_vector/sqrt(sum(line_vector.^2));

v1 = photPath(2:end,:) - photPath(1:end-1,:);
v1 = v1./sqrt(sum(v1.^2,2));
J= size(v1,1);
v2 = repmat(line_vector(:)',J,1);
v3 = cross(v1,v2);%
Vdiag = spalloc(J*3,J*3,9*J);

P = line_point-photPath(1:end-1,:);
P = P'; P = P(:);

for j = 1:J
	Vdiag(((j-1)*3+1):((j-1)*3+3),((j-1)*3+1)) = v1(j,:);
	Vdiag(((j-1)*3+1):((j-1)*3+3),((j-1)*3+2)) = v2(j,:);
	Vdiag(((j-1)*3+1):((j-1)*3+3),((j-1)*3+3)) = v3(j,:);
end
T = Vdiag\(P);
t1 = T(1:3:J*3);
t3 = T(3:3:J*3);
c = photPath(1:end-1,:) + t1.*v1;


ID_sqr = max_distance.^2;

% case i), find where 
kk = c-photPath(1:end-1,:); %c - a
I2 = any(0>kk.*v1,2);     % true if v1 and (c-a) are in the opposite direction, i.e. c is "infront" of a
kk = c-photPath(2:end,:); 
I3 = any(0<kk.*v1,2);     % true if v1 and (c-b) are in the same direction, i.e. c is "behind" of b
I1 = (~I2)&(~I3)&(sum(t3.^2,2)<ID_sqr); % true if c is inbetween a and b and 
I1 = [I1;false] | [false;I1];

% test if c = b or a is close enough to line (case ii and iii)
% s = (a-p2) - <(a-p2),v2>*v2, s is the shortest vector from a to the line
v2 = [v2;line_vector];
a_dist_sqr = (sum(((photPath-line_point) - dot((photPath-line_point),v2,2).*v2).^2,2));
I2 = a_dist_sqr<ID_sqr;


interactNodes = I1|I2;
