function [spectra,f] = noInteractSpectra(S,Fs)
% calculates the spectra when there is no ultrasound-light interaction,
% i.e. when the interaction distance of a path lies entirely outside the
% interaction distance (or what a normal diffuse tomography measurement would
% give). Recommendation: use SI units on inputs.
%
% Inputs:
% S		 - Number of points in the time/frequency vector. shopuld be a power
%		   of 2 for FFT considerations. No unit
% Fs     - Digital sampling frequency. Unit: Hz
%
% Outputs: 
% spectra - spectra with no interaction.
% f       - vector with the frequency for the spectra
% Author: David Hill 2022


s = (1:S);
t = (s-1-S/2)/Fs;
f = (s-1-S/2)*Fs/S; % zero-centered frequency range
E = Eo(t);

epsilon_0 = 8.854e-12;
c = 299792458;

dt = t(2)-t(1);
df = f(2)-f(1);
I = abs(fftshift(fft(E))).^2*c*epsilon_0;
spectra = I*dt/(S*df);
end

