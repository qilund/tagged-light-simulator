function [I,f] = ...
	tagged_spectra_simulator_smart(paths,S,Q,Fs,wl,parforMonitor)
% tagged_spectra_simulator
% calculates the ultrasonically tagged spectra from stored MC paths
% Inputs:  paramaters - A struct with fields necessary by the simulation.
%                       See the nested function 'testParamaters' for which
%                       fields in paramaters that are necessary.
%          paths      - A cell with M elements. Each cell index m cointains
%                       a [J_m,3] matrix with coordinates the scattering
%                       points of path m.
%
% Outputs: spectra    - A cell array with M power spectra computed by the
%                       simulation.
%          f          - A frequency vector in Hz. plot(f, spectra{m}) plots
%                       the m-th spectra with real frequency on the x-axis.
%
% Author: David Hill 2021.

if nargin< 6
    parforMonitor = false;
end

%% readability cleanup

%timeVector
s = (1:S); %s vector
t = (s-1-S/2)/Fs; % zero centered time span
f = (s-1-S/2)*Fs/S; % zero-centered frequency range

%wavenumber
k0=2*pi/wl;

% physical constants
epsilon_0 = 8.854e-12;
c = 299792458;
clear singlePacket
%interaction dists;
clear interaction_prediction % empty 
I_noInteract = abs(fftshift(fft(Eo(t)))).^2*c*epsilon_0;
%% initialization of variables


if iscell(paths)
	%total number of paths;
	M = numel(paths);
	if parforMonitor && ~isempty(ver('parallel'))
		ppm = ParforProgressbar(M,'title', 'tagged spectra progression');
	else
		ppm = dummyParforProgressbar();
	end
	I = cell(M,1);
	I = cellfun(@(x) zeros(numel(f),1),I,'UniformOutput',false);
	%% parfor loop
	% can be made into normal loop exchanging "parfor" to "for"
	for m = 1:M
		interactNodes = interaction_prediction(paths{m});
		if ~any(interactNodes)
			I{m} = I_noInteract;
		else
			I{m} = singlePacket(paths{m},t,interactNodes,Q,k0); % I_m in paper
		end
		ppm.increment();
	end
	I = cell2mat(I);
	delete(ppm);
else
	interactNodes = interaction_prediction(paths,US_r0,minIntDist);
	if ~any(interactNodes)
		I = I_noInteract;
	else
		I = singlePacket(paths,t,interactNodes,Q,k0); % I_m in paper
	end

end
%% fixing discreet energy conservation:
% Parsevals (discreet) theorem for time series x(n) and
% fourier transform X(k):
%  S                 S
% sum |x(k)|^2 =1/S sum |X(k)|^2
% n=1               k=1
%
% I is the discreet fourier transform of the discretized electric field but
% "real" energy is not conserved in respect to the underlying variables (t
% and f). From Parsevals theorem and the definitions in this simulator:
%                     S                                 S
% E_real = E_pulse = sum |E(k)|^2*dt*c*epsilon_0 = 1/S sum I(k)*df
%                    n=1                               k=1
% however,
%
%  S
% sum I_real(k)*df =  E_real aswell!
% k=1
%
% and thus I_real = I *dt / (S*df)
dt = t(2)-t(1);
df = f(2)-f(1);
I = I*dt/(S*df);
end

%% singlePacket function
function [singleSpectrum] = singlePacket(r_equilibrium,tVec,interactNodes,Q,k0)
% singlePacket - calculates the normalized spectrum of a single packet.
% The output is normalized to the numerical integral of the
%INPUTS:
% p             - paramater struct.
% r_equilibrium - undisturbed scattering points. Nx3 matrix
% tVec          - time vector
%
%OUTPUTS:
% singleSpectrum - normalized single packet spectrum

% physical constants
persistent epsilon_0 c use_mex
if isempty(c)
	epsilon_0= 8.854e-12;
	c = 299792458;
	use_mex = exist('usn_mex','file');
% 	use_mex = false;
end

% simplify notation

nInteractPaths = sum(interactNodes); % actually one more than the number of
%free paths that interact, balanced by j counting from 2 (i.e. j = 1 here is j = 0 in paper)

% Seperating spatial dimensions to their own variables for more flexible
% accoustic field functions. This is done as a group representation of the
% spatial dimensions as one variable (e.g. 'r') became cumbersome when
% representing scalar products in the function handle (e.g all 'x'
% coordinates on all free paths are 'xi(:,:,:,1)' (see further down for
% what 'xi' is)).
x = repmat(r_equilibrium(interactNodes,1),1,1,length(tVec));
y = repmat(r_equilibrium(interactNodes,2),1,1,length(tVec));
z = repmat(r_equilibrium(interactNodes,3),1,1,length(tVec));
% permute to make make first dimension of matrix time
x = permute(x,[3,1,2]);
y = permute(y,[3,1,2]);
z = permute(z,[3,1,2]);

% extend t to have same dimension as x,y,z
t = repmat(tVec(:),[1,nInteractPaths,1]);

%calculate movement of scatterers and new positions.
% Does not include first and last point.
tmp1 = x;
tmp2 = y;
x(:,2:end-1) = tmp1(:,2:end-1) + ...
	Adx(tmp1(:,2:end-1),tmp2(:,2:end-1),z(:,2:end-1),t(:,2:end-1));

y(:,2:end-1) = tmp2(:,2:end-1) + ...
	Ady(tmp1(:,2:end-1),tmp2(:,2:end-1),z(:,2:end-1),t(:,2:end-1));

z(:,2:end-1) = z(:,2:end-1) + ...
	Adz(x(:,2:end-1),y(:,2:end-1),z(:,2:end-1),t(:,2:end-1));


%kalculate optical propagation vectors (kj)
kx = x(:,2:end) - x(:,1:end-1);
ky = y(:,2:end) - y(:,1:end-1);
kz = z(:,2:end) - z(:,1:end-1);

%Normalizing
L = squeeze(sqrt(kx.^2 + ky.^2 + kz.^2)); %free path lengths
kx = kx./L; % normalized kx
ky = ky./L; % normalized ky
kz = kz./L; % normalized kz

%If statement to shorten computation if the refractive index function only
%returns a single value (i.e. constant refractive index).
%Calculate path step lengths for integration
dl = L/Q;

% initialize xi (in article xi = bold x)
xix = repmat(dl,1,1,Q+1);
xiy = repmat(dl,1,1,Q+1);
xiz = repmat(dl,1,1,Q+1);

% get vector of integers that is used subsequently
tmp1 = zeros(1,1,Q+1);
tmp1(1,1,:) = 0:Q;
tmp1 = repmat(tmp1,[size(L) 1]);
xix = xix.*tmp1;
xiy = xiy.*tmp1;
xiz = xiz.*tmp1;

%extending x
tmp1 = repmat(x(:,1:end-1),[1 1 Q+1]);

%finaliuzng xix
xix = tmp1 + kx.*xix;

% extending y
tmp1 = repmat(y(:,1:end-1),[1 1 Q+1]);

% fnalizing xiy
xiy = tmp1 + ky.*xiy;

% extending z
tmp1 = repmat(z(:,1:end-1),[1 1 Q+1]);

% fnalizing xiz
xiz = tmp1 + kz.*xiz;

% xi now contains integration points along each free path, separated with
% dl. xi's dimensions: [time FreePathNumber integrationPoint],
%          size of xi: [S, Jm , Q+1]

% extend t to incorporate the integration step points dimension
t3 = repmat(tVec(:),[1 size(xix,2) size(xix,3)]);

%phi =Accumulated phase as function of time
if use_mex
n = usn_mex(xix,xiy,xiz,t3);
else
n = usn(xix,xiy,xiz,t3);
end
int = trapz(n,3);
int_squeezed = squeeze(int.*dl);
phi = k0*sum(int_squeezed,2)';
% phi = k0*sum(squeeze(trapz(usn(xix,xiy,xiz,t),3).*dl),2);

%electric field
E_det = Eo(tVec).*exp(1i*phi);


singleSpectrum = abs(fftshift(fft(E_det))).^2*c*epsilon_0;
end
