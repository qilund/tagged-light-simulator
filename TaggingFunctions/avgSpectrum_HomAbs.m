function [expected_I] = avgSpectrum_HomAbs(spectra,paths,mu_a,T_det)
% avgSpectrumHomogeneousAbsorption
% calculates the average spectra with the homogenous absorption mu_a and
% path lengths D.
% Inputs:  spectra     - A cell array with the M spectra.
%          paths       - cell array with the M paths for the M spectra.
%          mu_a        - the homogeneous absorption coefficient.
%          T_det       - ratio of total number of initiated paths which are
%                        terminated in the detector.
%
% Outputs: expected_I  - the expectation value of the M paths.
%
% Author: David Hill 2021

D = zeros(size(paths)); % initialize D
parfor m=1:length(paths)
	D(m) = totalPathLength(paths{m});
end
I = cell(length(D),1);
Weights = exp(-mu_a*D);
parfor m = 1:length(D)
	I{m} = spectra{m}.*Weights(m);
end
M = numel(paths);
expected_I = T_det/M*sum(cell2mat(I'),2);
end

