function cleanupTaggedLightSimulator(UScase,lightCase)
%deletes any settings files, preparing for new simulation. Also clears all
%.m files in the lightCase and UScase directories (clearing any persistent
%variables).
%
% Inputs: UScase	- name of US case, e.g. 'simpleGaussianPulseYprop'
%		  lightCase - name of light case, e.g. 'unitAreaGaussianPulse' 
% 
% Author: David Hill 2022

tmp = mfilename('fullpath');
myDir = tmp(1:end-length(mfilename)-1);
if nargin ==0
	US_folders = split(genpath([myDir '\ultrasoundCases']),';');
	light_folders = split(genpath([myDir '\lightCases']),';');
else
	US_folders = {UScase};
	light_folders = {lightCase};
end
warning off
for i = 1:numel(US_folders)
	delete([US_folders{i} '\*.mat'])
	clear([US_folders{i} '\*.m'])
end
for i = 1:numel(light_folders)
	delete([light_folders{i} '\*.mat'])
	clear([light_folders{i} '\*.m'])
end
end

