function [interaction_nodes] = interaction_prediction(photon_path)
% INTERACTION_PREDICTION

persistent interaction_prediction_mode line_vector line_point center_point max_distance %#ok<PSET>
if isempty(interaction_prediction_mode)
	load("US_settings.mat",'interaction_prediction_mode') % should be on path after setupTaggedLightSimulator and writeUltrasoundSettings
end

switch interaction_prediction_mode
	case 'point'
		if isempty(center_point)
			load("US_settings.mat",'center_point','max_distance') % should be on path after setupTaggedLightSimulator and writeUltrasoundSettings
		end
		[interaction_nodes] = interaction_prediction_point(photon_path,...
			center_point,...
			max_distance);
	case 'line'
		if isempty(line_point)
			load("US_settings.mat",'line_point','line_vector','max_distance') % should be on path after setupTaggedLightSimulator and writeUltrasoundSettings
		end
		[interaction_nodes] = interaction_prediction_line(photon_path,...
			line_point,...
			line_vector,...
			max_distance);
	otherwise
		error('Erroneous interaction_prediction_mode in US_settings.mat')
end

