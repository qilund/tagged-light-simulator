function I = signalSelection(f,spectra,f_center,f_width)
% signalSelection integrates the signal in the spectra in a window with 
% width f_width and centered at f_center. Recommendation: use SI units.
%
%IPUTS: 
% f	- frequency vector
% spectra - vector with input intensity spectra
% f_center - center frequency of the selected frequency
% f_width - width of frequency window from which the signal gathered by
%	       integration of spectra around f_center.
%Outputs:
% I - integrated signal
%
%authors: David Hill 2022

df = f(2)-f(1);
[~, Icenterpos] = min(abs(f-f_center));
Iwidth = f_width/df;

Is = round(Icenterpos - Iwidth/2);
If = round(Icenterpos + Iwidth/2);
Ipos = trapz(df,spectra(:,Is:If),2);

[~, Icenterneg] = min(abs(f+f_center));
Is = round(Icenterneg - Iwidth/2);
If = round(Icenterneg + Iwidth/2);
Ineg = trapz(df,spectra(:,Is:If),2);

I = (Ipos + Ineg)'/2;

end

