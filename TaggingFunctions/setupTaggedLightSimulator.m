function setupTaggedLightSimulator(ultrasoundCase,lightCase)
% Sets up the tagged light simulator
%
% Inputs: UScase	- name of US case, e.g. 'simpleGaussianPulseYprop'
%		  lightCase - name of light case, e.g. 'unitAreaGaussianPulse' 
% 
% Author: David Hill 2022
if nargin == 0 || nargin == 1
    error('Please define ultrasound and light cases')
end
try
	pp = gcp;
	if isempty(pp)
		warning('no paralell pool created, tagged light simulator may be very slow.')
	end
catch ME
	warning('Parallel Computing Toolbox not installed.')
end



cleanupTaggedLightSimulator
tmp = mfilename('fullpath');
myDir = tmp(1:end-length(mfilename)-1);
if~exist(strcat(myDir, "\ultrasoundCases\", ultrasoundCase),"dir")
	error('Specified US case does not exist')
end
if~exist(strcat(myDir, "\lightCases\", lightCase),"dir")
	error('Specified light case does not exist')
end

warning('off')
rmpath(genpath([myDir '\ultrasoundCases']))
rmpath(genpath([myDir '\lightCases']))
addpath(strcat(myDir, "\lightCases\", lightCase))
delete(strcat(myDir, "\lightCases\", lightCase, '\*.mat'))

addpath(strcat(myDir, "\ultrasoundCases\", ultrasoundCase))
delete(strcat(myDir, "\ultrasoundCases\", ultrasoundCase,'\*.mat'))
warning on