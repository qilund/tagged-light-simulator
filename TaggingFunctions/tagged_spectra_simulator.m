function [I,f] = ...
    tagged_spectra_simulator(parameters,paths)
% tagged_spectra_simulator
% calculates the ultrasonically tagged spectra from stored MC paths
% Inputs:  paramaters - A struct with fields necessary by the simulation.
%                       See the nested function 'testParamaters' for which
%                       fields in paramaters that are necessary.
%          paths      - A cell with M elements. Each cell index m cointains
%                       a [J_m,3] matrix with coordinates the scattering
%                       points of path m.
%
% Outputs: spectra    - A cell array with M power spectra computed by the
%                       simulation.
%          f          - A frequency vector in Hz. plot(f, spectra{m}) plots
%                       the m-th spectra with real frequency on the x-axis.
%
%
%
% Author: David Hill 2021.

%% readability cleanup
p = parameters;
clearvars parameters;
p = testParamaters(p);

%timeVector
s = (1:p.S)'; %s vector
t = (s-1-p.S/2)/p.Fs; % zero centered time span
f = (s-1-p.S/2)*p.Fs/p.S; % zero-centered frequency range

% physical constants
epsilon_0 = 8.854e-12;
c = 299792458;

%% initialization of variables
if iscell(paths)
    %total number of paths;
    M = numel(paths);
    parfor_progress(M);
    I = cell(M,1);
    US_r0 = [p.US.x0 p.US.y0 p.US.z0];
    minIntDist = p.minInteractDist;
    I_noInteract = abs(fftshift(fft(p.ElectricFieldEnvelope(t)))).^2*c*epsilon_0;
    %% parfor loop
    % can be made into normal loop exchanging "parfor" to "for"
    parfor m = 1:M
        if ~any(interaction_prediction(paths{m},US_r0,minIntDist))
            I{m} = I_noInteract;
        else
            I{m} = singlePacket(p,paths{m},t); % I_m in paper
        end
        parfor_progress;
    end
    parfor_progress(0);
    
    %% fixing discreet energy conservation:
    % Parsevals (discreet) theorem for time series x(n) and
    % fourirer transform X(k):
    %  S                 S
    % sum |x(k)|^2 =1/S sum |X(k)|^2
    % n=1               k=1
    %
    % I is the discreet fourier transform of the discretizedelectric field but
    % "real" energy is not conserved in respect to the underlying variables (t
    % and f). From Parsevals theorem and the definitions in this simulator:
    %                     S                                 S
    % E_real = E_pulse = sum |E(k)|^2*dt*c*epsilon_0 = 1/S sum I(k)*dt
    %                    n=1                               k=1
    % however,
    %
    %  S
    % sum I_real(k)*df =  E_real aswell!
    % k=1
    %
    % and thus I_real = I *dt / (S*df)
    dt = t(2)-t(1);
    df = f(2)-f(1);
    I = cellfun(@(x) x*dt/(p.S*df),I,'UniformOutput', false);
else
    I = singlePacket(p,paths,t);
    dt = t(2)-t(1);
    df = f(2)-f(1);
    I = I*dt/(p.S*df);
end
end


%% testParamater Function
function p = testParamaters(p)
%testParamaters tests the input paramaters such that they include all
%necessary values.
%
%Inputs:    p - parameter struct
%
%OUTPUTS:   p - parameter struct (updated with booleans for special cases)
%




p.Q; % number of subdivisions used in the integration between
%scattering points. This is a constant in order to keep the matrices
%regular. Thus, the resolution varies slightly as the free path lengths differ.

p.k0;% optical wave number

p.scattererMovementX; % function handle for movement of particles in x,
p.scattererMovementY; % function handle for movement of particles in y,
p.scattererMovementZ; % function handle for movement of particles in z,
p.ElectricFieldEnvelope; % electric field envelope handle
p.refractiveIndex;% refractive index function handle,
p.minInteractDist;
testn = p.refractiveIndex(ones(10,10,10),ones(10,10,10),ones(10,10,10),ones(10,10,10));
try
    testn(10,10,10);
    p.singleValueRefractiveIndex = 0;
catch
    p.singleValueRefractiveIndex = 1;
end
end
%% singlePacket function
function [singleSpectrum] = singlePacket(p,r_equilibrium,tVec)
% singlePacket - calculates the normalized spectrum of a single packet.
% The output is normalized to the numerical integral of the
%INPUTS:
% p             - paramater struct.
% r_equilibrium - undisturbed scattering points. Nx3 matrix
% tVec          - time vector
%
%OUTPUTS:
% singleSpectrum - normalized single packet spectrum

% physical constants
epsilon_0 = 8.854e-12;
c = 299792458;

% simplify notation
dx = p.scattererMovementX;
dy = p.scattererMovementY;
dz = p.scattererMovementZ;
n  = p.refractiveIndex;
E  = p.ElectricFieldEnvelope;
nFreePaths = size(r_equilibrium,1); % actually one more than the number of
%free paths, balanced by j counting from 2 (i.e. j = 1 here is j = 0 in paper)

% Seperating spatial dimensions to their own variables for more flexible
% accoustic field functions. This is done as a group representation of the
% spatial dimensions as one variable (e.g. 'r') became cumbersome when
% representing scalar products in the function handle (e.g all 'x'
% coordinates on all free paths are 'xi(:,:,:,1)' (see further down for
% what 'xi' is)).
x = repmat(r_equilibrium(:,1),1,1,length(tVec));
y = repmat(r_equilibrium(:,2),1,1,length(tVec));
z = repmat(r_equilibrium(:,3),1,1,length(tVec));
% permute to make make first dimension of matrix time
x = permute(x,[3,1,2]);
y = permute(y,[3,1,2]);
z = permute(z,[3,1,2]);

% extend t to have same dimension as x,y,z
t = repmat(tVec,[1,nFreePaths,1]);

%calculate movement of scatterers and new positions.
% Does not include first and last point.
tmp1 = x;
tmp2 = y;
x(:,2:end-1) = tmp1(:,2:end-1) + ...
    dx(tmp1(:,2:end-1),tmp2(:,2:end-1),z(:,2:end-1),t(:,2:end-1));

y(:,2:end-1) = tmp2(:,2:end-1) + ...
    dy(tmp1(:,2:end-1),tmp2(:,2:end-1),z(:,2:end-1),t(:,2:end-1));

z(:,2:end-1) = z(:,2:end-1) + ...
    dz(x(:,2:end-1),y(:,2:end-1),z(:,2:end-1),t(:,2:end-1));


%kalculate optical propagation vectors (kj)
kx = x(:,2:end) - x(:,1:end-1);
ky = y(:,2:end) - y(:,1:end-1);
kz = z(:,2:end) - z(:,1:end-1);

%Normalizing
L = squeeze(sqrt(kx.^2 + ky.^2 + kz.^2)); %free path lengths
kx = kx./L; % normalized kx
ky = ky./L; % normalized ky
kz = kz./L; % normalized kz

%If statement to shorten computation if the refractive index function only
%returns a single value (i.e. constant refractive index).
if p.singleValueRefractiveIndex
    phi = p.k0*sum(n(1,1,1,1).*L,2);
else
    %Calculate path step lengths for integration
    dl = L/p.Q;
    
    % initialize xi (in article xi = bold x)
    xix = repmat(dl,1,1,p.Q+1);
    xiy = repmat(dl,1,1,p.Q+1);
    xiz = repmat(dl,1,1,p.Q+1);
    
    % get vector of integers that is used subsequently
    tmp1 = zeros(1,1,p.Q+1);
    tmp1(1,1,:) = 0:p.Q;
    tmp1 = repmat(tmp1,[size(L) 1]);
    xix = xix.*tmp1;
    xiy = xiy.*tmp1;
    xiz = xiz.*tmp1;
    
    %extending x
    tmp1 = repmat(x(:,1:end-1),[1 1 p.Q+1]);
    
    %finaliuzng xix
    xix = tmp1 + kx.*xix;
    
    % extending y
    tmp1 = repmat(y(:,1:end-1),[1 1 p.Q+1]);
    
    % fnalizing xiy
    xiy = tmp1 + ky.*xiy;
    
    % extending z
    tmp1 = repmat(z(:,1:end-1),[1 1 p.Q+1]);
    
    % fnalizing xiz
    xiz = tmp1 + kz.*xiz;
    
    % xi now contains integration points along each free path, separated with
    % dl. xi's dimensions: [time FreePathNumber integrationPoint],
    %          size of xi: [S, Jm , Q+1]
    
    % extend t to incorporate the integration step points dimension
    t = repmat(tVec(:),[1 size(xix,2) size(xix,3)]);
    
    %phi =Accumulated phase as function of time
    phi = p.k0*sum(squeeze(trapz(n(xix,xiy,xiz,t),3).*dl),2);
end
%electric field
E_det = E(tVec).*exp(1i*phi);


singleSpectrum = abs(fftshift(fft(E_det))).^2*c*epsilon_0;
end
