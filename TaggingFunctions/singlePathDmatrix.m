function D = singlePathDmatrix(path,edges,m)
% singlePathDmatrix - calculates the matrix D_m of the path m.
%
% Inputs:
% path  -
% edges -
% m -  number of path, used for error tracking.


CVC = [discretize(path(1,1),edges.x), ... % Current Voxel Coordinate
	discretize(path(1,2),edges.y), discretize(path(1,3),edges.z)];
b = [0 0 0];
last_i = size(path,1)-1;
D = zeros(numel(edges.x)-1,numel(edges.y)-1,numel(edges.z)-1);
maxCVC = size(D);

for i = 1:last_i
	s_min_has_been_zero = false;
	remDist = sqrt(sum((path(i+1,:)-path(i,:)).^2));
	if remDist == 0
		continue
	end
	k = (path(i+1,:)-path(i,:))./remDist;
	c = path(i,:);

	% extra error check, slows down function a bit if block is uncommented
% 	if i >1
% 		sumDmat = sum(D,'all');
% 		sumPath = sum(sqrt(sum((path(2:i,:)-path(1:i-1,:)).^2,2)));
% 		if abs(sumDmat-sumPath)/sumPath >1e-9
% 			error('Error in calculating segment %i. path index: %i',i-1,m)
% 		end
% 	end

	while remDist >0
		%the voxel 3 boundaries that the photon is heading for
		if k(1)>=0
			b(1) = edges.x(CVC(1)+1);
		else
			b(1) = edges.x(CVC(1));
		end
		if k(2)>=0
			b(2) = edges.y(CVC(2)+1);
		else
			b(2) = edges.y(CVC(2));
		end
		if k(3)>=0
			b(3) = edges.z(CVC(3)+1);
		else
			b(3) = edges.z(CVC(3));
		end
		s = abs((c-b)./k);
		%s are the 3 step distances to 3 voxel boundaries the photton is
		% heading for.
		[s_min,I] = min(s);

		% s_min = 0 should happen only once in a row (it can happen
		% "legally" if the initialization point and intial propagation is
		% along a voxel border. If it happens twice in a row then we have
		% some numerical error. This probably that remDist is almost 0.
		% Throwing error if the parry of this error doesn't work
		if s_min == 0
			if s_min_has_been_zero % if s_min has just been zero then throw error
				remDist = 0;
				if second_s_min_flag
					error('Unknown numerical error, path %i, segment %i',m,i)
				else
					second_s_min_flag = true;
				end
			end
			s_min_has_been_zero = true; % set flag that s_min was just zero
		else
			s_min_has_been_zero = false; % reset s_min being just zero flag
			second_s_min_flag = false;
		end
		% the shortest step the photon can take before hitting another voxel
		if s_min > remDist % step with s_min
			c = c + k*remDist;
			D(CVC(1),CVC(2),CVC(3)) = D(CVC(1),CVC(2),CVC(3)) + remDist;
			remDist = 0;
			continue

		else % step with remDist
			c = c + k*s_min;
			remDist = remDist - s_min;
			D(CVC(1),CVC(2),CVC(3)) = D(CVC(1),CVC(2),CVC(3)) + s_min;
		end
		% if we have stepped with s_min, go to the voxel that corresponds to this
		% step, i.e. the voxel I

		if k(I)>0
			if CVC(I) == maxCVC(I) % this is the last segment OR there is a reflection in the external boundary
				% cvc is the same in both cases.
			else
				% we move up one voxel.
				CVC(I) = CVC(I) + 1;
			end
		else
			if CVC(I) == 1 % this is the last egment OR there is a reflection in the external boundary, e.g. edges.x(1).
				% CVC is the same in the reflection case and it doesnt matter in the
				% other.
			else
				CVC(I) = CVC(I) -1;
			end
		end

	end

end

sumDmat = sum(D,'all');
sumPath = sum(sqrt(sum((path(2:end,:)-path(1:end-1,:)).^2,2)));

if abs(sumDmat-sumPath)/sumPath >1e-9
	error('Length of path %i does not match total distance in D matrix.',m)
end
end