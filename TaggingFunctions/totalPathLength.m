function D = totalPathLength(path)
% totalPathLength calculates total length of path, size(Path) must be equal
% to [J, 3].
% Inputs:  path   - [J, 3] sized matrix with coordinates;
%
% Outputs: D      - total length of path (same unit as path coordinates)
%
% Author: David Hill 2021
if size(path,2) ==3

D = sum(sqrt(sum((path(2:end,:) - path(1:end-1,:)).^2,2)));
else
    error('Wrong dimension of ''paths'', maybe it just needs to be transposed?')
end
end