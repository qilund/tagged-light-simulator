function varargout = plotLightPulse(S,Fs)
% Plots the optical pulse found in the current light case. 
% Inputs:
% S		 - Number of points in the time/frequency vector. shopuld be a power
%		   of 2 for FFT considerations. No unit
% Fs     - Digital sampling frequency. Unit: Hz
%
% Outputs: 
% t	     - time vector.
% E      - Electric field vs time in t
% I		 - Optical intensity vs time in t
%
% Author: David Hill 2022
	

try
Eo(0);
catch ME
	error('Light case not found, have you run setupTaggedLightSimulator? If yes, double check that E0 and writeLightSettings is correctly defined.')
end
epsilon = 8.854e-12;
c = 299792458;
if nargin == 0
t = linspace(-5e-6,5e-6,1e3);
else
	s = (1:S)'; %s vector
	t = (s-1-S/2)/Fs;
end
E = Eo(t);
figure
I = E.^2*c*epsilon;
yyaxis left
plot(t,E);
xlabel('t [us]')
ylabel('Electric field amplitude [v/m]')
yyaxis right
plot(t,I)
ylabel('Power [W]')

varargout{1}  =t;
varargout{2}  =E;
varargout{3}  =I;


end

